﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Utility  {
	
	public static List<E> ShuffleList<E>(List<E> inputList)
	{
		List<E> randomList = new List<E>();

		//		Random r = new Random();
		int randomIndex = 0;
		while (inputList.Count > 0)
		{
			randomIndex = Random.Range(0, inputList.Count); //Choose a random object in the list
			randomList.Add(inputList[randomIndex]); //add it to the new, random list
			inputList.RemoveAt(randomIndex); //remove to avoid duplicates
		}

		return randomList; //return the new random list
	}

	public static void DebugLog(object obj){
		#if DEBUG
		Debug.Log(obj);
		#endif
	}

	public static void DebugError(object obj){
		#if DEBUG
		Debug.LogError(obj);
		#endif
	}
}

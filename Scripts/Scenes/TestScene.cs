﻿using UnityEngine;
using System.Collections;

public class TestScene : MonoBehaviour {

	public void OnShowBannerAdsClick(){
		AdsManager.GetInstancce ().ShowBannerAds (AdsManager.AdProvider.ADMOB);
	}

	public void OnShowRewardedVideoClick(){
		AdsManager.GetInstancce ().ShowVideoRewardAd (delegate (bool isSucced) {
			
		});
	}

	public void OnGaClick(){
		GoogleAnalyticsV4.getInstance ().LogEvent ("login", "abc", "xyz", 1);
		GoogleAnalyticsV4.getInstance ().LogScreen ("test screen");
		GoogleAnalyticsV4.getInstance ().LogException ("dkmkmkmkkm", true);
		GoogleAnalyticsV4.getInstance ().LogItem ("transid", "name", "sku?", "category1", 123, 1);
		GoogleAnalyticsV4.getInstance ().LogSocial ("socialnetwork1", "socialaction1", "social target1");
		GoogleAnalyticsV4.getInstance ().LogTiming ("timing category", 123, "timing name", "timing label");
		GoogleAnalyticsV4.getInstance ().LogTransaction ("transid", "affilitation", 123, 1234, 12345, "currency code");
	}
}

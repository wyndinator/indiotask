﻿using UnityEngine;
using System.Collections;

public class LoginPanelController : MonoBehaviour {

	public delegate void OnSocialLoginFinish(SocialResult result);
	public OnSocialLoginFinish OnSocialLoginFinishCallback = null;

	public delegate void OnLoggedInSocial();
	public OnLoggedInSocial OnLoggedInSocialCallBack = null;
    

    public void OnFacebookLoginClick(){
		LoadingManager.getInstance ().Show ();
		if ( UserController.GetInstance ().DidLoggedIn ()) {
			//			dialogManager.getInstanceDialogMana ().showDialog (GameObject.Find ("Canvas").transform, "DidLoggedIn");
			OnLoggedInSocialCallBack();
		} else {
			SocialManager.GetInstance ().Initilalize (SocialPlatform.FACEBOOK, OnInitSocialCallBack);
		}
    }

	void OnInitSocialCallBack(SocialResult result){
		if (result.IsSuccess) {
			SocialManager.GetInstance ().Login (new System.Collections.Generic.List<string>{"public_profile", "email", "user_friends" }, OnLoginSocialCallBack);
		} else {
			OnLoginSocialFail (result);
		}
	}

	void OnLoginSocialCallBack(SocialResult result){
		if (result.IsSuccess) {
			SocialManager.GetInstance ().GetUserInfo (OnGetInfoSocialCallBack);
		} else {
			OnLoginSocialFail (result);
		}
	}

	void OnGetInfoSocialCallBack(SocialResult result){
		if (result.IsSuccess) {
			OnSocialLoginFinishCallback (result);
		} else {
			OnLoginSocialFail (result);
		}
	}
		

	void OnLoginSocialFail(SocialResult result){
		LoadingManager.getInstance ().Hide ();
		Utility.DebugLog (result.Message);
		OnSocialLoginFinishCallback (result);
	}

	public void Show(){
		this.gameObject.SetActive (true);
	}

	public void Dismiss(){
		this.gameObject.SetActive (false);
	}

	public bool IsShowing(){
		return gameObject.activeInHierarchy;
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayGamePanelController : MonoBehaviour {
	static int _nextCounter=0;
	static int _ansQuizCounter = 0;

	public QuizAnsScrollViewController _quizAnsScrollView;
	public AvatarDownloader _MyPlayerAvatar, _quizOwnerAvatar, _quizImage;
	public Text _quizOwnerText, _quizcontentText, _myPlayerNameText;
	public QuizStatsPanel _quizStatPanel;
	public GameObject _quizAnsPanel;
	public Button _reportButton, _likeButton;
	public SloganViewController _sloganController;

	int _currentQuizAns, _currentUserQuizAns;
	QuizModel _currentQuiz;

    void Start () {
	}

	void OnGetQuizCallBack(bool result, ServerResponseModel model){
		_MyPlayerAvatar.DownloadWithFbID (UserController.GetInstance ().UserModel.FacebookID);
		if (model.IsSuccess ()) {
			GetQuizResponseModel resModel = (GetQuizResponseModel)model;
			if (resModel.ResponseModelData.Count < 1) {
				ShowNoQuizLeft ( resModel.Message);
				return;	
			}
			foreach (var item in resModel.ResponseModelData) {
				QuizController.GetInstance ().AddQuiz (item);
			}

			ShowQuiz ();

			LoadingManager.getInstance().Hide();
		} else {
			LoadingManager.getInstance().Hide();
//			DialogManager.getInstanceDialogMana().showDialog("Failed to load
		}
	}

	void OnGetQuizStatisticCallBack(bool result, ServerResponseModel model){

		LoadingManager.getInstance ().Hide ();
		if (model.IsSuccess ()) {
			QuizStatisticResponseModel resModel = (QuizStatisticResponseModel)model;
			_quizAnsPanel.SetActive (false);
			_quizStatPanel.Show ();
			_quizStatPanel.UpdateStats (resModel.StatisticData, _currentUserQuizAns , _currentQuizAns, _currentQuiz.Explain);
		} else {
			DialogManager.getInstanceDialogMana ().showDialog (model.Message, dialog.TypeDialog.ONE_BUTTON_OK);
		}
	}

	void OnReportQuizCallBack(bool result, ServerResponseModel model){
		DialogManager.getInstanceDialogMana ().showDialog (model.Message, dialog.TypeDialog.ONE_BUTTON_OK);
	}


	void OnDestroy(){
		QuizController.GetInstance ().Save ();
	}

	public void Show(){
		this.gameObject.SetActive (true);
		if (QuizController.GetInstance ().HasQuizLeft ()) {
			OnNextButtonClick ();
		} else {
			ResetUI ();
			LoadingManager.getInstance().Show();
			_myPlayerNameText.text = UserController.GetInstance ().UserModel.FullName;
			GlobalServices.GetQuizFromSever (QuizController.GetInstance ().LastQuizIndex, OnGetQuizCallBack);
		}
		_sloganController.StartShowChat (UserController.GetInstance ().SloganConfigModel._listSlogan);
	}

	public void Hide(){
		this.gameObject.SetActive (false);
	}

	#region behavior


	#endregion

	#region UI event

	public void OnLikeQuizButtonClick(){
		_likeButton.interactable = false;
		GlobalServices.LikeQuiz (_currentQuiz.ID, delegate(bool arg1, ServerResponseModel arg2) {
			_likeButton.interactable = true;
		});
	}

	public void OnNextButtonClick(){

		if (QuizController.GetInstance ().HasQuizLeft ()) {
			ShowQuiz ();
		} else {
			LoadingManager.getInstance ().Show ();
			GlobalServices.GetQuizFromSever (QuizController.GetInstance ().LastQuizIndex, OnGetQuizCallBack);
		}
		_nextCounter++;
		if (_nextCounter % 3 == 0) {
			AdsManager.GetInstancce ().ShowInterstitialAds (AdsManager.AdShowAction.NEXT_QUIZ);
		}
	}

	public void OnQuizAnsClick(QuizAnsScrollViewItem ans){

		LoadingManager.getInstance ().Show ();
		int quizID = ans.getID ();
		GlobalServices.AnsQuiz (_currentQuiz.ID, ans.getID(), null);
		GlobalServices.GetQuizStatistic (_currentQuiz.ID, OnGetQuizStatisticCallBack);
		_currentUserQuizAns = ans.getID ();
		_ansQuizCounter++;
		if (_ansQuizCounter % 5 == 0) {
			AdsManager.GetInstancce ().ShowInterstitialAds (AdsManager.AdShowAction.ANS_QUIZ);
		}
	}

	public void OnReportButtonClick(){
		_reportButton.interactable = false;
		GlobalServices.ReportQuiz (_currentQuiz.ID, delegate(bool arg1, ServerResponseModel arg2) {
			_reportButton.interactable = true;
		});
	}
	#endregion

	#region UI update

	void UpdatePlayerAvatar(){
		
	}

	void ShowQuiz(){
		QuizModel quiz = QuizController.GetInstance ().GetQuiz ();
		QuizController.GetInstance ().RemoveLastQuiz ();
		_quizStatPanel.Hide ();
		_quizAnsPanel.SetActive (true);
		_quizAnsScrollView.GenerateItem (quiz.GetListAns ());
		_quizOwnerText.text = quiz.Author;
		_quizcontentText.text = quiz.Content;
		if (quiz.HasImage()) {
			_quizImage.gameObject.SetActive (true);
			_quizImage.Download (quiz.ImageUrl,false);
		} else
			_quizImage.gameObject.SetActive (false);

		_quizOwnerAvatar.DownloadWithFbID (quiz.AuthorFacebookID);
		_currentQuiz= quiz;
		_currentQuizAns = quiz.Answer;
	}

	void ShowNoQuizLeft(string message){
		DialogManager.getInstanceDialogMana ().showDialog (message);
		LoadingManager.getInstance ().Hide ();
	}

	void ResetUI(){
		_quizStatPanel.Hide ();
		_quizAnsPanel.gameObject.SetActive (false);
		_quizOwnerText.text = "";
		_quizcontentText.text = "";

	}
	#endregion
}

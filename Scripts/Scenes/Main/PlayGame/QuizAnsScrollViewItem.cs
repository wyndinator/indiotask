﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuizAnsScrollViewItem : MonoBehaviour {
	public ScrollRect _itemContentScrollRect;
	int _id;
	public Text _content, _quizIndexString;
	public float _maxItemHeight;
	public LayoutElement _layoutElement;
	public Image _itemRaycastBlock;

	public int getID(){
		return _id;
	}

	public void UpdateContent(int id, string content){
		_id = id;
		_content.text = content;
		StartCoroutine ("WaitUpdateSize");

		switch (id) {
		case 0:
			_quizIndexString.text = "A";
			break;
		case 1:
			_quizIndexString.text = "B";
			break;
		case 2:
			_quizIndexString.text = "C";
			break;
		case 3:
			_quizIndexString.text = "D";
			break;
		}
	}

	IEnumerator WaitUpdateSize(){
		yield return new WaitForEndOfFrame ();

		if (_content.preferredHeight > _maxItemHeight) {
//			_layoutElement.enabled = true;
			_itemRaycastBlock.raycastTarget= true;
			_layoutElement.preferredHeight = _maxItemHeight;
			_itemContentScrollRect.enabled = true;
		} else {
//			_layoutElement.enabled = false;
			_itemContentScrollRect.enabled = false;
			_itemRaycastBlock.raycastTarget= false;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuizStatisticItem : MonoBehaviour {
	public Text _statisticText;
	public Image _statsImage, _myAnswerImage, _serverAnswer;

	public void UpdateData(int stats, int totalStat, bool isMyAnswer, bool isRightAnswer){
//		if (stats == 0) {
//			gameObject.SetActive (false);
//			return;
//		}

		gameObject.SetActive (true);
		float percent;
		if (totalStat == 0)
			percent = 0;
		else
			percent = (float)stats / (float)totalStat;

		percent *= 100.0f;
		_statisticText.text = System.String.Format("{0:0}", percent)+ "%";
		_myAnswerImage.gameObject.SetActive (isMyAnswer);
		_serverAnswer.gameObject.SetActive (isRightAnswer);
//		_statsImage.fillAmount = percent;
	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class QuizAnsScrollViewController : MonoBehaviour {
	public QuizAnsScrollViewItem _sampleItem;

	List<QuizAnsScrollViewItem> _itemPool = new List<QuizAnsScrollViewItem>();

	// Use this for initialization
	void Start () {
		_sampleItem.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GenerateItem(List<string> ans){
		HideAll ();
		for (int i = 0; i < ans.Count; i++) {
			QuizAnsScrollViewItem item = GetItem ();
			item.gameObject.SetActive (true);
			string text = ans[i];
			item.UpdateContent (i, text);
		}
	}

	void HideAll(){
		foreach (var item in _itemPool) {
			item.gameObject.SetActive (false);
		}
	}

	QuizAnsScrollViewItem GetItem(){
		foreach (var item in _itemPool) {
			if (!item.gameObject.activeInHierarchy)
				return item;
		}

		QuizAnsScrollViewItem newItem = Instantiate<QuizAnsScrollViewItem> (_sampleItem);
		newItem.transform.SetParent (transform);
		newItem.transform.localPosition = Vector3.zero;
		newItem.transform.localScale = Vector3.one;
		_itemPool.Add (newItem);

		return newItem;
	}
}

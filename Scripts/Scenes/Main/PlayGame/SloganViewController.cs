﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SloganViewController : MonoBehaviour {
	public Text _chatText;
	bool _isShowing = false;
	List<string> _listSlogan;
	int _currentIndex;
	float _timeElapsed = 0;

	public void StartShowChat(List<string> listSlogan){
		_listSlogan = listSlogan;
		gameObject.SetActive (true);
		_isShowing = true;
		_timeElapsed = 0;
		_timeElapsed = 15;
	}


	void Update(){
		_timeElapsed += Time.deltaTime;

		if (_timeElapsed > 15) {
			_timeElapsed = 0;

			int nextIndex=0;
			if (_listSlogan.Count > 1) {
				nextIndex = Random.Range (0, _listSlogan.Count);
				while (nextIndex == _currentIndex) {
					nextIndex = Random.Range (0, _listSlogan.Count);
				}

				_currentIndex = nextIndex;
			}

			_chatText.text = _listSlogan [_currentIndex];
		}
	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class QuizStatsPanel : MonoBehaviour {
	public QuizStatisticItem _item0, _item1, _item2, _item3;
	public Text _explainText;

	public void UpdateStats(QuizStatisticModel model, int userAns, int rightAns, string explainAns){
		HideAllQuizStatItem ();
		int total = model.Option0 + model.Option1 + model.Option2 + model.Option3;
		_item0.UpdateData (model.Option0, total, userAns==0, rightAns==0);
		_item1.UpdateData (model.Option1, total, userAns==1, rightAns==1);
		_item2.UpdateData (model.Option2, total, userAns==2, rightAns==2);
		_item3.UpdateData (model.Option3, total,  userAns==3,rightAns==3);
		_explainText.text = explainAns;
	}

	public void Show(){
		this.gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	public void HideAllQuizStatItem(){
		_item0.gameObject.SetActive (false);
		_item1.gameObject.SetActive (false);
		_item2.gameObject.SetActive (false);
		_item3.gameObject.SetActive (false);
	}
}

﻿using UnityEngine;
using System.Collections;

public class MainSceneController : MonoBehaviour {
	
	[SerializeField]
	private LoginPanelController _loginPanel;
	[SerializeField]
	private GameObject _mainPanel;

	static int _backToMainCounter=0;

	public PlayGamePanelController _playGamePanel;
	public PostQuizPanelController _postQuizPanel;
	public HelpPanelController _helpPanel;
	public GameObject _aboutPanel;

	// Use this for initializationwefe

	void Start () {
//		if (UserController.GetInstance ().DidLoggedIn ()) {
//			ShowDialogMessage ("logged in");
//		} else {
//			GlobalServices.RegisterAccount ("EAAX7Eb2oZBcYBAIaaFKdYKSe9qYn29BINKrwbSidSq58eWIZCZAt27sSYiZBeBoqjymrh8agRUZBMU58yAG6rZClQVzEK0AhGRYuwWSZCySCUN0XYx9byOffZAmssiHTw6PRnJVifF943HNzBKSMwK04EaJZBgF6EAuUZC1khPAD4aHGEbNypsanZBFFHbjIm68t3MZD",
//				delegate(bool arg1, ServerResponseModel arg2) {
//					if (!arg1) {
//						ShowDialogMessage ("Failed to login");
//					} else {
//						Debug.Log(arg2.ToJson());
//						ShowDialogMessage("success");
//						UserController.GetInstance().UserModel = ((ServerRegisterResponseModel)arg2).ResponseModelData;
//						UserController.GetInstance().SaveUserLoginInfo();
//						_loginPanel.gameObject.SetActive (false);
//						_mainPanel.gameObject.SetActive (true);
//					}
//				});
//		}

//
		DisableAllPanel ();
		_loginPanel.Show ();
		_loginPanel.OnSocialLoginFinishCallback += OnSocialLoginFinishCallback;
		_loginPanel.OnLoggedInSocialCallBack += OnLoggedInSocialCallBack;

		if ( UserController.GetInstance ().DidLoggedIn ()) {
			_loginPanel.OnFacebookLoginClick ();
		}

		StartCoroutine ("DelayLoadAds");
	}

	IEnumerator DelayLoadAds(){
		yield return null;

		AdsManager.GetInstancce ().LoadInterstitialAds ();
	}

	public string _testJson;
	void TestJson(){
		GetConfigResponseModel model = new GetConfigResponseModel ();
		model.UpdateFromJson (_testJson);
	}


	public void OnTestClick(){
		LoadingManager.getInstance().Show();
	}


	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) 
			OnAndroidBack(); 
	}

	#region UI update

	void  DisableAllPanel(){
		_mainPanel.SetActive (false);
		_loginPanel.gameObject.SetActive (false);
		_playGamePanel.gameObject.SetActive (false);
		_postQuizPanel.Hide ();
		_aboutPanel.SetActive (false);
		_helpPanel.Hide ();
	}

	void ShowDialogMessage(string messsage){
//		DialogManager.getInstanceDialogMana ().showDialog (GameObject.Find ("Canvas").transform, messsage);
	}

	#endregion

	#region UI events

	public void OnPlayGameButtonclick(){
		
		DisableAllPanel ();
		_playGamePanel.Show ();
	}

	public void OnPostQuizButtonclick(){
		DisableAllPanel ();
		_postQuizPanel.Show ();
	}	

	public void OnHelpButtonclick(){
		_mainPanel.SetActive (false);
		_helpPanel.Show ();
	}	

	public void OnAboutButtonclick(){
		_mainPanel.SetActive (false);
		_aboutPanel.SetActive (true);
	}

	public void OnPostQuizBackClick(){
		_postQuizPanel.Hide ();
		_mainPanel.gameObject.SetActive (true);
	}

	public void OnPlayGamezBackClick(){
		_playGamePanel.Hide ();
		_mainPanel.gameObject.SetActive (true);

		_backToMainCounter++;
		if (_backToMainCounter % 5 == 0) {
			AdsManager.GetInstancce ().ShowInterstitialAds (AdsManager.AdShowAction.BACK_MENU  );
		}
	}

	public void OnAboutFanpageClick(){
		Application.OpenURL (UserController.GetInstance().AboutConfig.fanpage);
	}

	public void OnAboutBackClick(){
		_aboutPanel.SetActive (false);
		_mainPanel.SetActive (true);
	}

	public void OnHelpBackClick(){
		_mainPanel.SetActive (true);
		_helpPanel.Hide ();
	}

	public void OnAndroidBack(){
		if (_loginPanel.IsShowing () || _mainPanel.activeInHierarchy)
			Application.Quit ();
	}

	public void OnPolicyClick(){
		Application.OpenURL (UserController.GetInstance ().PolicyLink);
	}
	#endregion


	#region behavior
	void OnSocialLoginFinishCallback(SocialResult result){
		if (result.IsSuccess){
			
			GlobalServices.RegisterAccount (SocialManager.GetInstance ().UserInfoModel.Token,
				delegate(bool arg1, ServerResponseModel arg2) {
					if (!arg1 || !arg2.IsSuccess ()) {
						LoadingManager.getInstance().Hide();
						ShowDialogMessage ("Failed to login");
					} else {
						UserController.GetInstance().UserModel= ((ServerRegisterResponseModel)arg2).ResponseModelData;
						UserController.GetInstance().SaveUserLoginInfo();
						OneSignalController.GetInstance().TagUser(UserController.GetInstance().UserModel.FacebookID);
						_loginPanel.gameObject.SetActive (false);
						_mainPanel.gameObject.SetActive (true);
						LoadingManager.getInstance().Hide();

						if (!UserController.GetInstance().IsSameConfig()){
							LoadingManager.getInstance().Show();
							GlobalServices.GetConfigServer(OnGetConfigCallBack);
						}
						else{
							AdsManager.GetInstancce().InitAdsActionRandom(UserController.GetInstance().AdsConfig);
						}

						GlobalServices.GetGameNews(OnGetGameNewsCallBack);
					}
				});

		}else{
			ShowDialogMessage ("Failed to login!");
		}
	}

	void OnLoggedInSocialCallBack(){
		GlobalServices.LoginToServer (delegate(bool arg1, ServerResponseModel arg2) {
			if (arg1 && arg2.IsSuccess ()) {
				UserController.GetInstance().UserModel= ((ServerRegisterResponseModel)arg2).ResponseModelData;
				UserController.GetInstance().SaveUserLoginInfo();
				OneSignalController.GetInstance().TagUser(UserController.GetInstance().UserModel.FacebookID);
				_loginPanel.gameObject.SetActive (false);
				_mainPanel.gameObject.SetActive (true);
				LoadingManager.getInstance().Hide();
				if (!UserController.GetInstance().IsSameConfig()){
					LoadingManager.getInstance().Show();
					GlobalServices.GetConfigServer(OnGetConfigCallBack);
				}
				else{
					AdsManager.GetInstancce().InitAdsActionRandom(UserController.GetInstance().AdsConfig);
				}

				GlobalServices.GetGameNews(OnGetGameNewsCallBack);
			} else {

				if (arg2.Code==GameMessResponseCode.EXPIRED_TOKEN){
					UserController.GetInstance().Reset();
					_loginPanel.OnFacebookLoginClick();
				}else{
					LoadingManager.getInstance().Hide();
					ShowDialogMessage ("Failed to login!");
				}
			}
		});
	}

	void OnGetConfigCallBack(bool isSuccess, ServerResponseModel md){
		if (isSuccess) {

			LoadingManager.getInstance().Hide();
			GetConfigResponseModel model = (GetConfigResponseModel)md;
			UserController.GetInstance ().AdsConfig = model.Data.ads;
			UserController.GetInstance ().UpdateHelpModel(model.Data.help);
			UserController.GetInstance ().UpdateListSlogan(model.Data.slogan);
			UserController.GetInstance ().AboutConfig = model.Data.about;
			UserController.GetInstance ().AdsConfig.SaveToLocal ();
			UserController.GetInstance ().PolicyLink = model.Data.policy;
			UserController.GetInstance ().SaveConfig ();
			AdsManager.GetInstancce ().InitAdsActionRandom (UserController.GetInstance ().AdsConfig);
		}
	}

	void OnGetGameNewsCallBack(bool isSuccess, ServerResponseModel md){
		if (isSuccess) {
			LoadingManager.getInstance().Hide();
			GetGameNewsResponseModel model = (GetGameNewsResponseModel)md;
			foreach (var item in model.Data) {
				string tmp = item.Url;
				if (string.IsNullOrEmpty (tmp)) {
					DialogManager.getInstanceDialogMana ().showDialog (item.Content, dialog.TypeDialog.ONE_BUTTON_OK);
				} else {
					DialogManager.getInstanceDialogMana ().showDialog (item.Content, dialog.TypeDialog.TWO_BUTTON, delegate() {
						Application.OpenURL (tmp);
					});
				}

			}
		}
	}
	#endregion


}

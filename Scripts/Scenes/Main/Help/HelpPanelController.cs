﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HelpPanelController : MonoBehaviour {
	public List<AvatarDownloader> _listHelpImage;

	int currentIndex = 0;

	// Use this for initialization
	void Start () {
	}

	public void Show(){
		gameObject.SetActive (true);

		foreach (var item in _listHelpImage) {
			item.gameObject.SetActive (true);
			item.gameObject.transform.localPosition = new Vector3 (9999, 0, 0);
		}

		_listHelpImage [0].transform.localPosition = Vector3.zero;
//		_listHelpImage [0].Download (UserController.GetInstance ().HelpConfig.urlImage0, false);
//		_listHelpImage [1].Download (UserController.GetInstance ().HelpConfig.urlImage1, false);
//		_listHelpImage [2].Download (UserController.GetInstance ().HelpConfig.urlImage2, false);
//		_listHelpImage [3].Download (UserController.GetInstance ().HelpConfig.urlImage3, false);
	}

	void HideAllListHelp(){
		foreach (var item in _listHelpImage) {
			item.gameObject.transform.localPosition = new Vector3 (9999, 0, 0);
		}
	}

	public void Hide(){
		gameObject.SetActive (false);
	}


	public void OnForwardClick(){
//		foreach (var item in _listHelpImage) {
//			//			item.transform.localPosition = new Vector3 (1000, item.transform.localPosition.y, 0);
//			item.gameObject.SetActive(false);
//		}
		currentIndex++;
		if (currentIndex >= _listHelpImage.Count)
			currentIndex = _listHelpImage.Count - 1;
		//		_listHelpImage [currentIndex].transform.localPosition = new Vector3 (384, _listHelpImage [currentIndex].transform.localPosition.y, 0);
		HideAllListHelp();
		_listHelpImage [currentIndex].gameObject.transform.localPosition = Vector3.zero;
	}

	public void OnBackwardClick(){
//		foreach (var item in _listHelpImage) {
////			item.transform.localPosition = new Vector3 (1000, item.transform.localPosition.y, 0);
//			item.gameObject.SetActive(false);
//		}

		currentIndex--;
		if (currentIndex < 0)
			currentIndex = 0;
//		_listHelpImage [currentIndex].transform.localPosition = new Vector3 (384, _listHelpImage [currentIndex].transform.localPosition.y, 0);
		HideAllListHelp();
		_listHelpImage [currentIndex].gameObject.transform.localPosition = Vector3.zero;
	}

}

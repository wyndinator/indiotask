﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PostQuizAnsScrollView : MonoBehaviour {
	public float _maxHeight;
	// Use this for initialization
	public LayoutElement _layoutElems;
	public RectTransform _contentRectTrans;

	//	public GameObject _andsItemSample;
	public List<GameObject> _itemPool = new List<GameObject>();
	public List<GameObject> _listCheckBox = new List<GameObject>();


	void Start () {
	}


	void UpdateSize(){
		StartCoroutine ("WaitUpdateSize");

	}

	IEnumerator WaitUpdateSize(){
		yield return null;
//		Debug.Log ("fdslf");
		if (_contentRectTrans.sizeDelta.y > _maxHeight) {
			_layoutElems.preferredHeight = _maxHeight;
		} else {
			_layoutElems.preferredHeight = _contentRectTrans.sizeDelta.y;
		}	
	}

	public bool CanAddAnsItem(){
		foreach (var item in _itemPool) {
			if (!item.activeInHierarchy) {
				return true;
			}
		}

		return false;
	}

	public void AddQuizAnsItem(){
		for (int i = 0; i < _itemPool.Count; i++) {
			if (!_itemPool[i].gameObject.activeInHierarchy) {
				_itemPool[i].SetActive (true);
				_listCheckBox [i].SetActive (false);
				UpdateSize ();
				return;
			}
		}
//		foreach (var item in _itemPool) {
//			if (!item.gameObject.activeInHierarchy) {
//				item.SetActive (true);
//				UpdateSize ();
//				return;
//			}
//		}
	}

	void HideAllItem(){
		foreach (var item in _itemPool) {
			item.SetActive (false);
		}

		foreach (var item in _listCheckBox) {
			item.SetActive (false);
		}
	}

	public void ShowAnsItem(int numberItemToShow){
		HideAllItem ();
		for (int i = 0; i < numberItemToShow; i++) {
			_itemPool [i].SetActive (true);
		}

		UpdateSize ();
	}

	public void HideAllCheckBox(){
		foreach (var item in _listCheckBox) {
			item.SetActive (false);
		}
	}

	public void ShowCheckBox(int index){
		_listCheckBox [index].SetActive (true);
	}

	public bool IsShowingCheckBox(int index){
		return _listCheckBox [index].activeInHierarchy;
	}
}

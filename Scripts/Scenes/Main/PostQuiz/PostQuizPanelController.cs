﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PostQuizPanelController : MonoBehaviour {
	public InputField _quizContent, _ans1, _ans2, _ans3, _ans4, _quizExplain;
	public Button _addAnsButton, _postQuizButton;
	public PostQuizAnsScrollView _ansScrollView;

	int _currentQuizCorrectAnsIndex = 0;
	// Use this for initialization
	void Start () {
		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void Show(){
		gameObject.SetActive (true);
		Reset ();
		_addAnsButton.interactable = CanAddAnsItem();
		_currentQuizCorrectAnsIndex = 0;
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	public void Reset(){
		_ans1.text = "";
		_ans2.text = "";
		_ans3.text = "";
		_ans4.text = "";
		_quizExplain.text = "";
		_quizContent.text = "";
		_addAnsButton.interactable = CanAddAnsItem();
		_postQuizButton.interactable = CanPostQuiz ();
		_ansScrollView.ShowAnsItem (2);
	}

	#region ui events

	public void OnAddAnsClick(){
		_ansScrollView.AddQuizAnsItem ();
	}

	public void OnOkPostQuizClick(){
		_postQuizButton.interactable = false;
		GlobalServices.PostQuiz (PostQuizCallBack, " ", _quizContent.text, _ans1.text, _ans2.text, _ans3.text, _ans4.text, _quizExplain.text, _currentQuizCorrectAnsIndex, 0);
	}

	void PostQuizCallBack(bool isSuccess, ServerResponseModel model){
		_postQuizButton.interactable = true;
		if (model.Code == GameMessResponseCode.SUCCESS) {
			DialogManager.getInstanceDialogMana ().showDialog (model.Message, dialog.TypeDialog.ONE_BUTTON_OK);
			Reset ();
		} else {
			DialogManager.getInstanceDialogMana ().showDialog (model.Message, dialog.TypeDialog.ONE_BUTTON_OK);
		}
	}

	public void OnAnsInputFieldEndEdit(){
		_addAnsButton.interactable = CanAddAnsItem();
		_postQuizButton.interactable = CanPostQuiz ();
	}

	public void OnCheckBoxClick(int index){
		if (_currentQuizCorrectAnsIndex != index) {
			_ansScrollView.HideAllCheckBox ();
			_ansScrollView.ShowCheckBox (index);
			_currentQuizCorrectAnsIndex = index;
		} else {
			if (_ansScrollView.IsShowingCheckBox (index)) {
				_ansScrollView.HideAllCheckBox ();
				_currentQuizCorrectAnsIndex = 0;
			}else{
				_ansScrollView.ShowCheckBox (index);
				_currentQuizCorrectAnsIndex = index;
			}
		}

	}

	#endregion

	bool CanAddAnsItem(){
		if (_ans4.gameObject.activeInHierarchy)
			return false;

		if (_ans3.gameObject.activeInHierarchy && _ans3.text != "")
			return true;

		if (_ans1.text != "" && _ans2.text != "")
			return true;
		else
			return false;
	}

	bool CanPostQuiz(){
		return (_quizContent.text != "" && _ans1.text != "" && _ans2.text != "");
	}
}

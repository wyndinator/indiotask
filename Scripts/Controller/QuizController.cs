﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class QuizController
{
	List<QuizModel> _listQuiz = new List<QuizModel>();
	
	static QuizController _instance;
	public static QuizController GetInstance(){
		if (_instance == null) {
			_instance = new QuizController ();
			_instance.Init ();
		}

		return _instance;
	}

	int _lastQuizIndex;
	public int LastQuizIndex
	{
		get { return _lastQuizIndex; }
		set { _lastQuizIndex = value; }
	}

	public void Init(){

//		_lastQuizIndex = PlayerPrefs.GetInt (GameLocalDataKey.LastQuizIndex, 0);
		_lastQuizIndex = 0;
	}

	public void AddQuiz(QuizModel quiz){
		_listQuiz.Add (quiz);
		_lastQuizIndex++;
	}

	public bool HasQuizLeft(){
		return _listQuiz.Count > 0;
	}

	public QuizModel GetQuiz(){
		if (HasQuizLeft ()) {
			return _listQuiz [0];
		}

		return null;
	}

	public void RemoveLastQuiz(){
		_listQuiz.RemoveAt (0);
	}

	public void Save(){
		PlayerPrefs.SetInt (GameLocalDataKey.LastQuizIndex, _lastQuizIndex);
		PlayerPrefs.Save ();
	}
}


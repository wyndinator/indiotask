﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UserController  {

	UserModel _userModel;
	public UserModel UserModel
	{
		get { return _userModel; }
		set { _userModel = value; }
	}

	AdConfigModel _adsConfigModel;
	public AdConfigModel AdsConfig
	{
		get { return _adsConfigModel; }
		set { _adsConfigModel = value; }
	}

	HelpConfigModel _helpConfigModel;
	public HelpConfigModel HelpConfig
	{
		get { return _helpConfigModel; }
		set { _helpConfigModel = value; }
	}

	SloganConfigModel _sloganConfigModel;
	public SloganConfigModel SloganConfigModel
	{
		get { return _sloganConfigModel; }
		set { _sloganConfigModel = value; }
	}

	AboutConfigModel _aboutConfig;
	public AboutConfigModel AboutConfig
	{
		get { return _aboutConfig; }
		set { _aboutConfig = value; }
	}

	string _policyLink;
	public string PolicyLink{
		get{ return _policyLink; }
		set{ _policyLink = value; }
	}

	static UserController _instance;
	public static UserController GetInstance(){
		if (_instance == null){
			_instance = new UserController ();
			_instance.Init ();
		}
		return _instance;
	}



	void Init(){
		_userModel = new UserModel ();
		_userModel.UserServerToken = PlayerPrefs.GetString (GameLocalDataKey.UserServerToken, "");
		_userModel.FacebookID = PlayerPrefs.GetString (GameLocalDataKey.FacebookID, "");
		_userModel.ConfigVersion = PlayerPrefs.GetInt (GameLocalDataKey.ConfigVersion, 0);
		_policyLink= PlayerPrefs.GetString (GameLocalDataKey.ConfigPolicyLink, "https://sites.google.com/site/hoinguonline/home/policy");

		_helpConfigModel = new HelpConfigModel ();
		_helpConfigModel.LoadFromLocal ();

		_aboutConfig = new AboutConfigModel ();
		_aboutConfig.LoadFromLocal ();

		_adsConfigModel = new AdConfigModel ();
		_adsConfigModel.LoadFromLocal ();

		_sloganConfigModel = new SloganConfigModel ();
		_sloganConfigModel.LoadFromLocal ();
	}

	public void Reset(){
		_userModel.UserServerToken = "";
		_userModel.FacebookID = "";
		SaveUserLoginInfo ();
	}

	public bool DidLoggedIn(){
		return _userModel.UserServerToken.Length > 1 && _userModel.FacebookID.Length > 1;
	}

	public void SaveUserLoginInfo(){
		PlayerPrefs.SetString (GameLocalDataKey.UserServerToken, _userModel.UserServerToken);
		PlayerPrefs.SetString (GameLocalDataKey.FacebookID, _userModel.FacebookID);
		PlayerPrefs.SetInt (GameLocalDataKey.ConfigVersion, _userModel.ConfigVersion);
	}

	public void SaveConfig(){

		PlayerPrefs.SetString (GameLocalDataKey.ConfigPolicyLink, _policyLink);
		_aboutConfig.SaveToLocal ();
		_helpConfigModel.SaveToLocal ();
		_adsConfigModel.SaveToLocal ();
		_sloganConfigModel.SaveToLocal ();

		PlayerPrefs.Save ();
	}

	public bool IsSameConfig(){
		return false;
//		return PlayerPrefs.GetInt (GameLocalDataKey.ConfigVersion) == UserModel.ConfigVersion;
	}

	public void UpdateHelpModel(List<string> listHelp){
		_helpConfigModel = new HelpConfigModel ();
		_helpConfigModel .urlImage0 = listHelp [0];
		_helpConfigModel .urlImage1 = listHelp [1];
		_helpConfigModel .urlImage2 = listHelp [2];
		_helpConfigModel .urlImage3 = listHelp [3];
	}

	public void UpdateListSlogan(List<string> listSlogan){
		_sloganConfigModel.UpdateFromList (listSlogan);
	}
}

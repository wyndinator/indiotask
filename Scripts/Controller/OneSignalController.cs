﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OneSignalController : MonoBehaviour {

	string _currentID, _currentPushToken;

	static OneSignalController _instance;
	public static OneSignalController GetInstance(){
		return _instance;
	}

	void Start () {
		// Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
		// OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);

		_instance = this;
		OneSignal.StartInit("8ddbf93c-c170-47f2-81d9-f01494891da1")
			.HandleNotificationOpened(HandleNotificationOpened)
			.EndInit();

		OneSignal.IdsAvailable(IdsAvailable);

		// Call syncHashedEmail anywhere in your app if you have the user's email.
		// This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
		// OneSignal.syncHashedEmail(userEmail);
	}

	// Gets called when the player opens the notification.
	private static void HandleNotificationOpened(OSNotificationOpenedResult result) {
		Debug.Log (result.notification);
	}

	public static void Push(){
		Dictionary<string, object> abc = new Dictionary<string, object> ();
//		abc.Add ("key", "value");
		OneSignal.PostNotification (abc);
	}

	private void IdsAvailable(string userID, string pushToken) {
		_currentID = userID;
		_currentPushToken = pushToken;
	}

	public string GetID(){
		return _currentID;
	}

	public void TagUser(string fbID){
		OneSignal.SendTag("Facebookid", fbID);
	}
}

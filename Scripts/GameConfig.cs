﻿using UnityEngine;
using System.Collections;

public static class GameConfig  {
//	http://52.35.67.156/
//	public static string Url = "http://52.35.67.156:8080";
//	public static int Port = 8080;

	public static string GameVersion = "1.0";



}

public class GameCommand{
	public static string Register = "/api/user/register";
	public static string Login = "/api/user/login";
	public static string PostQuiz = "/api/question/post";
	public static string AnsQuiz = "/api/question/answer";
	public static string GetQuiz = "/api/question/get";
	public static string GetStats = "/api/question/getStatic";
	public static string LikeQuiz = "/api/question/like";
	public static string ReportQuiz = "/api/question/report";
	public static string UpdateOneSignal = "/api/user/updateOnesignalID";
	public static string GetGameConfig = "/api/game/getConfig";
	public static string GetGameNews = "/api/game/getNews";
}

public class GameMessResponseCode{
	public static int SUCCESS = 1;
	public static int EXPIRED_TOKEN = -103;
}

public class GameLocalDataKey{
	public static string UserServerToken = "UserServerToken";
	public static string FacebookID = "FacebookID";
	public static string ConfigVersion = "ConfigVersion";
	public static string LastQuizIndex = "LastQuizIndex";


	public static string HelpUrlImage0 = "HelpUrlImage0";
	public static string HelpUrlImage1 = "HelpUrlImage1";
	public static string HelpUrlImage2 = "HelpUrlImage2";
	public static string HelpUrlImage3 = "HelpUrlImage3";


	public static string AboutConfigCharacter = "AboutConfigCharacter";
	public static string AboutConfigFanpage = "AboutConfigFanpage";
	public static string AboutConfigCopyright = "AboutConfigCopyright";

	public static string AdsConfigNextThree = "AdsConfigNextThree";
	public static string AdsConfigBackToMenu= "AdsConfigBackToMenu";
	public static string AdsConfigPassFive = "AdsConfigPassFive";


	public static string SloganConfigItem = "SloganConfigItem";
	public static string TotalSloganItemCount = "TotalSloganItemCount";


	public static string ConfigPolicyLink = "ConfigPolicyLink";

}

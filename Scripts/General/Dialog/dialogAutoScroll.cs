﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class dialogAutoScroll : MonoBehaviour {

    public float ystart;
    public bool isScrool = false;
    public float timeDefault = 1f;
    public float timeDontScroll = -1;
    public float timeScrollTo = 0.2f;
    public Text txt;

    // Use this for initialization
    void Awake()
    {

        txt = gameObject.GetComponentInChildren<Text>();
        ystart = txt.transform.position.y;
    }
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        
            if (gameObject.transform.position.y != ystart)
            {
                timeDontScroll += Time.deltaTime;
                if (timeDontScroll > timeDefault)
                {
                    timeDontScroll = 0;
                    //gameObject.transform.position = new Vector3 (gameObject.transform.position.x, ystart, gameObject.transform.position.z);
                    //txt.transform.position=new Vector3(txt.transform.position.x,ystart,txt.transform.position.z);
                    iTween.MoveTo(txt.gameObject, new Vector3(txt.transform.position.x, ystart, txt.transform.position.z), timeScrollTo);
                }
            }
        
    }
    public void onChangeValue()
    {
        isScrool = true;
        timeDontScroll = 0;
    }
}

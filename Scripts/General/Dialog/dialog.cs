﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 huyht: de khoi tao can call ham setTextInfo, setTypeDialog
     */
public class dialog : MonoBehaviour
{
	public enum TypeDialog
	{
		TWO_BUTTON,
ONE_BUTTON_OK,
ONE_BUTTON_CANCLE
	}

	private TypeDialog typeDialog;
	private System.Action actionOk;
	private System.Action actionCancle;
	private Button btnOk;
	private Button btnCancle;
	public Text txtInfo;
	private float timeShowScale = 0.6f;
	private float timeToScale = 0.1f;
	private float sizeScaleClick = 0.8f;
    public bool isShow;

    public void setAction (System.Action actionOk, System.Action actionCancle)
	{
		this.actionOk = actionOk;
		this.actionCancle = actionCancle;
	}

	public void setTypeDialog (TypeDialog type)
	{
		this.typeDialog = type;
		btnOk.gameObject.SetActive(typeDialog == TypeDialog.TWO_BUTTON);
		btnCancle.gameObject.SetActive(typeDialog == TypeDialog.TWO_BUTTON);

//		if (typeDialog != TypeDialog.TWO_BUTTON) {
//			btnOk.gameObject.SetActive(false);
//			btnCancle.gameObject.SetActive(false);
//		}
	}

	public bool isStarted = false;
	public bool isAwake = false;

	void Awake ()
	{
		isAwake = true;
		txtInfo = gameObject.GetComponentInChildren<Text> ();
		var allButton = gameObject.GetComponentsInChildren<Button> ();
		for (int i = 0; i < allButton.Length; i++) {
			if (allButton [i].name == "btnOk") {
				btnOk = allButton [i];
			}
			if (allButton [i].name == "btnCancle") {
				btnCancle = allButton [i];
			}
		}
	}

	public void setTextInfo (string textInfo)
	{
//		if(txtInfo==null)
//			txtInfo = gameObject.GetComponentInChildren<Text> ();
		txtInfo.text = textInfo;
	}

	// Use this for initialization
	void Start ()
	{
		//scaleToOff (false);
	}

	public void onClickOk ()
	{
		iTween.ScaleTo (btnOk.gameObject, iTween.Hash ("scale", new Vector3 (sizeScaleClick, sizeScaleClick, 0), "oncomplete", "finishClickOk", "oncompletetarget", gameObject, "time", timeToScale));
	}

	public void onClickCancle ()
	{
		iTween.ScaleTo (btnCancle.gameObject, iTween.Hash ("scale", new  Vector3 (sizeScaleClick, sizeScaleClick, 0), "oncomplete", "finishClickCancle", "oncompletetarget", gameObject, "time", timeToScale));
	}

	public void finishClickOk ()
	{
		iTween.ScaleTo (btnOk.gameObject, iTween.Hash ("scale", new Vector3 (1f, 1f, 0), "oncomplete", "callBackOk", "oncompletetarget", gameObject, "time", timeToScale));
	}

	public void finishClickCancle ()
	{
		iTween.ScaleTo (btnCancle.gameObject, iTween.Hash ("scale", new Vector3 (1f, 1f, 0), "oncomplete", "callBackCancle", "oncompletetarget", gameObject, "time", timeToScale));
	}

	public void callBackOk ()
	{
		scaleToOff (true);
		if (actionOk != null)
			actionOk ();
	}

	public void callBackCancle ()
	{
		scaleToOff (true);
		if (actionCancle != null)
			actionCancle ();
	}
	


	public void scaleToOff (bool isOff)
	{
        isShow = !isOff;
        int varSclae = isOff ? 0 : 1;
		List<GameObject> allGameObject = getAllChildOb ();
//		iTween.ScaleTo (gameObject, iTween.Hash ("scale", new Vector3 (varSclae, varSclae, 0), "oncomplete", "callBackScale", "oncompletetarget", gameObject, "time", timeShowScale));
		for (int i = 0; i < allGameObject.Count; i++) {
			//iTween.ScaleTo (allGameObject [i], new Vector3 (varSclae, varSclae, 0), timeShowScale);
			if (i != 0)
				iTween.ScaleTo (allGameObject [i], new Vector3 (varSclae, varSclae, 0), timeShowScale);
			else
				iTween.ScaleTo (allGameObject [i], iTween.Hash ("scale", new Vector3 (varSclae, varSclae, 0), "oncomplete", "callBackScale", "oncompletetarget", gameObject, "time", timeShowScale));
		}
	}

//	public void scaleToOff (bool isOff, System.Action callback)
//	{
//        isShow = !isOff;
//        int varSclae = isOff ? 0 : 1;
//		List<GameObject> allGameObject = getAllChildOb ();
//		iTween.ScaleTo (gameObject, new Vector3 (varSclae, varSclae, 0), timeShowScale);
////		for (int i = 0; i < allGameObject.Count; i++) {
////			if (i != 0)
////				iTween.ScaleTo (allGameObject [i], new Vector3 (varSclae, varSclae, 0), timeShowScale);
////			else
////				iTween.ScaleTo (btnCancle.gameObject, iTween.Hash ("scale", new Vector3 (varSclae, varSclae, 0), "oncomplete", "callBackScale", "oncompletetarget", gameObject, "time", timeShowScale));
////		}
//	}

	public void callBackScale ()
	{
		if (typeDialog == TypeDialog.ONE_BUTTON_OK) {
			oneButton (true);
		} else if (typeDialog == TypeDialog.ONE_BUTTON_CANCLE) {
			oneButton (false);
		}
	}
    public void showDialog()
    {
		isShow = true;
		gameObject.SetActive (true);
        scaleToOff(false);
    }


    public List<GameObject> getAllChildOb ()
	{
		List<GameObject> listGame = new List<GameObject> ();
		Transform[] children = gameObject.GetComponentsInChildren<Transform> ();

		foreach (Transform tr in children) {
			listGame.Add (tr.gameObject);
		}
		return listGame;
	}

	public void oneButton (bool isOk)
	{
		gameObject.SetActive (isShow);
		
		if (isOk) {
			//GameObject.Destroy (btnCancle.gameObject);
			btnCancle.gameObject.SetActive(false);
			btnOk.gameObject.SetActive(true);
			//float widthGameover = ((RectTransform)(gameObject.transform)).rect.width;
			//float widthBtnOk = ((RectTransform)(btnOk.gameObject.transform)).rect.width;
			//float positionX = gameObject.transform.position.x;
			btnOk.gameObject.transform.localPosition = new Vector3 (0, btnOk.transform.localPosition.y, btnOk.transform.localPosition.z);
			//btnOk.gameObject.transform.position.x=gameObject.transform.position.x;
		} else {
			//GameObject.Destroy (btnOk.gameObject);
			btnOk.gameObject.SetActive(false);
			btnCancle.gameObject.SetActive(true);
			btnCancle.gameObject.transform.position = new Vector3 (0, btnCancle.transform.position.y, btnCancle.transform.position.z);
		}
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DialogManager : MonoBehaviour {
    private List<dialog> listDialog = new List<dialog>();
    private Canvas mainCanvas;
	private static DialogManager instanceDialog = null;
    void Awake()
    {
    }
    //	public DialogManager(Transform rootTranfrom)
    //	{
    //		mainCanvas = rootTranfrom.root.GetComponent<Canvas> ();
    //	}
    public void init(Transform rootTranfrom)
    {
        mainCanvas = rootTranfrom.root.GetComponent<Canvas>();
    }
	public static DialogManager getInstanceDialogMana()
    {
        //		if (instanceDialog == null) {
        //			instanceDialog = new DialogManager();
        //		}
        //		return instanceDialog;

        if (instanceDialog == null)
        {
            GameObject go = new GameObject("DialogManager");
            DontDestroyOnLoad(go);
			go.AddComponent<DialogManager>();
			Transform canvas = GameObject.Find ("Canvas").transform;
			go.transform.SetParent (canvas);
			go.transform.localScale = Vector3.one;
//			mainCanvas = go.transform;
			instanceDialog = go.GetComponent<DialogManager>();
        }
        return instanceDialog;
    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }


	public void showDialog(string content, dialog.TypeDialog type = dialog.TypeDialog.ONE_BUTTON_OK, System.Action okCallBack = null, System.Action cancelCallBack = null)
    {
		if (string.IsNullOrEmpty (content))
			return;
//        mainCanvas = tranformDialog.root.GetComponent<Canvas>();
        dialog dialogshow = getDialogFree();
        dialogshow.showDialog();
		dialogshow.setTextInfo(content);
		dialogshow.setTypeDialog(type);
		dialogshow.setAction (okCallBack, cancelCallBack);

    }
    public dialog getDialogFree()
    {
        for (int i = 0; i < listDialog.Count; i++)
        {
            if (!listDialog[i].isShow)
            {
                return listDialog[i];
            }
        }
        Object prefab = Resources.Load("dialogShow");
        GameObject clone = Instantiate(prefab, Vector3.zero, Quaternion.identity) as GameObject;
		clone.transform.parent = gameObject.transform;
        clone.transform.localPosition = new Vector3(0, 0, 0);
        clone.transform.localScale = new Vector3(0, 0, 0);
        dialog myComponent = clone.GetComponent(typeof(dialog)) as dialog;
        listDialog.Add(myComponent);
        return myComponent;
    }

}

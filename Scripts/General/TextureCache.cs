﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public class TextureCache{
    const int MAX_CACHE_SIZE = 100;
    const float MAX_LIFE_TIME = 300.0f; // 5 minutes
    const int COUNTER_LIMIT =5;

    string prefixPath = "";
    Color[] transparentColors = new Color[100 * 100];

    Dictionary<string, TextureCacheRecord> _cachePool;
    // cache sprite
    Dictionary<string, SpriteCacheRecord> _cacheSpritePool;
	Sprite _defaultSprite;
    static TextureCache _instance;
    public static TextureCache GetInstance()
    {
        if (_instance == null)
        {
            _instance = new TextureCache();
            _instance.Init();
        }
        return _instance;
    }
   
    void Init()
    {
        prefixPath = Application.persistentDataPath + "/Save/";
        CreateFolder();

//		_defaultSprite = null;
        _cachePool = new Dictionary<string, TextureCacheRecord>();
        _cacheSpritePool = new Dictionary<string, SpriteCacheRecord>();


        Color transparentColor = new Color32(0,0,0,0);
        for (int i = 0; i < 100*100; i++)
        {
            transparentColors[i] = transparentColor;
        }
    }

    public void CreateFolder()
    {
        string pathFolder = Application.persistentDataPath + "/Save";
        if (!Directory.Exists(pathFolder))
        {
            Directory.CreateDirectory(pathFolder);
        }
    }

    public void AddTexture(string path, Texture2D res)
    {
        if (_cachePool.ContainsKey(path))
        {
            //update current one
            _cachePool[path].UpdateResource(res);
        }
        else
        {
            if (_cachePool.Count < MAX_CACHE_SIZE)
                _cachePool.Add(path, new TextureCacheRecord(res));
            ClearIfExceedCacheSize();
        }

//        SaveTexture(res, path);
    }

    public Texture2D GetTexture(string path)
    {
        if (_cachePool.ContainsKey(path))
        {
            return _cachePool[path].GetResouce();
        }
        else
        {
			return new Texture2D (10, 10);
        }
    }

	public Sprite GetDefaultSprite(){
		return _defaultSprite;
	}
    public Sprite GetSprite(string path)
    {
        return _cacheSpritePool[path].GetResouce();
        //if (_cacheSpritePool.Count< MAX_CACHE_SIZE)
        //{
        //    if (!_cacheSpritePool.ContainsKey(path))
        //    {
        //        // create new sprite
        //        Texture2D text = GetTexture(path);
        //        Sprite s = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
        //        _cacheSpritePool.Add(path, new SpriteCacheRecord(s));
        //    }

        //    return _cacheSpritePool[path].GetResouce();
        //}
        //else
        //{
        //    Texture2D text = GetTexture(path);
        //    return Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
        //}
    }
    
    public void AddSprite(Texture2D text, string path, bool doMaskAvatar = true)
    {
        if (!_cacheSpritePool.ContainsKey(path))
		{
			Sprite newSprite;

			if (text == null) {
				#if DEBUG
				Debug.Log("use default sprite for avatar with path: "+path);
				#endif
				newSprite = _defaultSprite;

			} 
			else 
			{
				if (doMaskAvatar)
					text = ManualMaskAvatar(text);

				//if (text.width != text.height)
				//    Debug.Log("fefw " + path);


				if (text.width == text.height)
				{
					newSprite= Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
				}
				else
				{
					if (doMaskAvatar)
					{
						int smallerDimension = text.width > text.height ? text.height : text.width;
						newSprite = Sprite.Create(text, new Rect(0, 0, smallerDimension, smallerDimension), new Vector2(0.5f, 0.5f));
					}
					else
					{
						newSprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
					}
				}

			}
			_cacheSpritePool.Add(path, new SpriteCacheRecord(newSprite));
        }
    }

    Texture2D ManualMaskAvatar(Texture2D srcText)
    {
        //theres alot of comments here because of a big fcking gigantic story behind it, contact dev for more info

        ////scale in case width and height too different
        //float diff = (float)srcText.width / srcText.height;
        //if (srcText.width != srcText.height)
        //{
        //    int biggerDimension = srcText.width > srcText.height ? srcText.height : srcText.width;
        //    TextureScale.Bilinear(srcText, biggerDimension, biggerDimension);
        //}


        if (srcText.width < 69 || srcText.height < 69)
        {
            TextureScale.Bilinear(srcText, 100, 100);
        }

        //bool isSupportSetPixel = srcText.format == TextureFormat.ARGB32 || srcText.format == TextureFormat.Alpha8;
        //bool isSquare = srcText.width == srcText.height;

        //if (true||isSupportSetPixel && isSquare)
        //{


        int smallerDimension = srcText.width > srcText.height ? srcText.height : srcText.width;
            Texture2D resultText = new Texture2D(smallerDimension, smallerDimension, TextureFormat.ARGB32, false);
            Color[] tm1p = srcText.GetPixels(0, 0, smallerDimension, smallerDimension);
            resultText.SetPixels(0, 0, smallerDimension, smallerDimension, tm1p);
            resultText.Apply();

            int r = resultText.width / 2;
            float rPow2 = r * r;

            int heightDelete, yStartUpper, tmp;
            for (int i = 0; i < r; i++)
            {
                tmp = r - i;

                heightDelete = r - (int)Mathf.Sqrt(rPow2 - tmp * tmp);
                yStartUpper = r - heightDelete;

                resultText.SetPixels(i, 0, 1, heightDelete, transparentColors);
                resultText.SetPixels(resultText.width - i - 1, 0, 1, heightDelete, transparentColors);

                resultText.SetPixels(i, r + yStartUpper, 1, heightDelete - 1, transparentColors);
                resultText.SetPixels(resultText.width - i - 1, r + yStartUpper, 1, heightDelete - 1, transparentColors);
            }
            resultText.SetPixels(0, 0, resultText.width, 1, transparentColors);
            resultText.SetPixels(0, resultText.height -2, resultText.width, 2, transparentColors);
            resultText.Apply();
            

            return resultText;
        //}
        //else
        //{
        //    return srcText;
            //Texture2D resultText = new Texture2D(srcText.width, srcText.height);

            //resultText.SetPixels(0, 0, srcText.width, srcText.height, transparentColors);

            //int smallerDimension = srcText.width > srcText.height ? srcText.height : srcText.width;

            //int r = smallerDimension / 2;
            //float rPow2 = r * r;
            //int maxWidth = smallerDimension;

            //int heightDelete, yStartUpper, tmp;

            //for (int i = 0; i <= r; i++)
            //{
            //    tmp = r - i;

            //    heightDelete = (int)Mathf.Sqrt(rPow2 - tmp * tmp);
            //    yStartUpper = r - heightDelete;

            //    resultText.SetPixels(i, r - heightDelete, 1, heightDelete * 2, srcText.GetPixels(i, r - heightDelete, 1, heightDelete * 2), 0);
            //    resultText.SetPixels(maxWidth - i - 1, r - heightDelete, 1, heightDelete * 2, srcText.GetPixels(maxWidth - i - 1, r - heightDelete, 1, heightDelete * 2), 0);
            //}
            //////dont really necessary since user cant notice the different
            ////if (smallerDimension % 2 != 0)
            ////{
            ////    resultText.SetPixels(smallerDimension / 2 + 1, 0, 1, r, srcText.GetPixels(smallerDimension / 2 + 1, 0, 1, r));
            ////}
            //resultText.Apply();

            //return resultText;
        //}
    }

    public bool ContainSprite(string path)
    {
        return _cacheSpritePool.ContainsKey(path);
    }

	public bool ContainTexture(string path)
    {
        if (_cachePool.ContainsKey(path))
        {
            return true;
        }
        else
        {
            if (IsExistTexture(path))
            {
                ClearIfExceedCacheSize();
                _cachePool.Add(path, new TextureCacheRecord(LoadTexture(path)));
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    bool IsExistTexture(string path)
    {
        string cutPath = CutPath(path);
        return (File.Exists(prefixPath + cutPath));
    }

    void SaveTexture(Texture2D text, string path)
    {
        string cutPath = CutPath(path);
        File.WriteAllBytes(prefixPath + cutPath, text.EncodeToPNG());
    }

    Texture2D LoadTexture(string path)
    {
        string cutPath = CutPath(path);
        byte[] bytes;
        bytes = File.ReadAllBytes(prefixPath + cutPath);
        Texture2D text = new Texture2D(1, 1);
        text.LoadImage(bytes);
        return text;
    }

    public void ClearCache(bool isClearAll = false)
    {
        if (isClearAll)
        {
            _cachePool.Clear();
            _cacheSpritePool.Clear();
        }
        else
        {
            float now = Time.realtimeSinceStartup;

            //var e = _cachePool.GetEnumerator();
            //while (e.MoveNext())
            //{
            //    if (now - e.Current.Value._lastAccess > MAX_LIFE_TIME && e.Current.Value._accessCounter < COUNTER_LIMIT)
            //        _cachePool.Remove(e.Current.Key);
            //}

            var spriteE = _cacheSpritePool.GetEnumerator();
            Dictionary<string, SpriteCacheRecord> tmp = new Dictionary<string, SpriteCacheRecord>(_cacheSpritePool);
            while (spriteE.MoveNext())
            {
                if (now - spriteE.Current.Value._lastAccess > MAX_LIFE_TIME && spriteE.Current.Value._accessCounter < COUNTER_LIMIT)
                {
                    tmp.Remove(spriteE.Current.Key);
                    AvatarDownloader.GetListDownloading().Remove(spriteE.Current.Key);
                }
            }
            _cacheSpritePool = tmp;
            
        }
    }
    
    //notes: only delete ONE element due to sync problem, this function call every time an element is added to the pool
    void ClearIfExceedCacheSize()
    {
        foreach (var item in _cachePool)
        {
            if (_cachePool.Count < MAX_CACHE_SIZE)
                break;
            _cachePool.Remove(item.Key);
            break; 
        }
    }

    string CutPath(string path)
    {
        string cutPath = "";

        //
        char[] pathArr = path.ToCharArray();
        int startIndex = -1;
        int endIndex = -1;
        for (int i = path.Length - 1; i >= 0; i--)
        {
            if (pathArr[i] == '.')
            {
                endIndex = i;
            }
            // get start index
            if (pathArr[i] == '/')
            {
                startIndex = i;
                break;
            }
        }
        //
        if (startIndex == -1)
            return cutPath;
        startIndex++;
        cutPath = path.Substring(startIndex, endIndex + 4 - startIndex);
        return cutPath;
    }

    class TextureCacheRecord
    {
        public Texture2D _resource;
        public float _lastAccess;
        public int _accessCounter;

        public TextureCacheRecord(Texture2D res)
        {
            _resource = res;
            _lastAccess = Time.realtimeSinceStartup;
            _accessCounter = 1;
        }

        public Texture2D GetResouce()
        {
            _accessCounter++;
            return _resource;
        }

        public void UpdateResource(Texture2D newRes)
        {
            _accessCounter++;
            _resource = newRes;
        }
    }

    class SpriteCacheRecord
    {
        public Sprite _resource;
        public float _lastAccess;
        public int _accessCounter;

        public SpriteCacheRecord(Sprite res)
        {
            _resource = res;
            _lastAccess = Time.realtimeSinceStartup;
            _accessCounter = 1;
        }

        public Sprite GetResouce()
        {
            _accessCounter++;
            _lastAccess = Time.realtimeSinceStartup;
            return _resource;
        }

        public void UpdateResource(Sprite newRes)
        {
            _accessCounter++;
            _resource = newRes;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using MovementEffects;

[ExecuteInEditMode]
public class GameObjectUtils : MonoBehaviour {
	//https://phet.colorado.edu/sims/curve-fitting/curve-fitting_en.html

	static GameObjectUtils _instance;
	static GameObjectUtils GetInstance()
	{
		if (_instance == null) {
			GameObject tmp = new GameObject ("GameObjectUtils");
			tmp.AddComponent<GameObjectUtils> ();
			_instance = tmp.GetComponent<GameObjectUtils> ();
//			DontDestroyOnLoad (_instance.gameObject);
		}
		return _instance;
	}

	public static void Init(){
		GetInstance ();
	}

	void Start(){
	}

	public static void CurveMoveTo(GameObject obj, float desX, float a, float b, float c, float d, float e, float time, float delay=0.0f, System.Action<object> callBack = null)
	{
		GetInstance().StartCoroutine(MoveToCurve (obj, desX, a, b, c, d, e, time, delay, callBack));
	}


//	public static void DoButtonClickEffect(GameObject obj){
//		iTween.ScaleTo(obj, iTween.Hash("x", 0.75f, "y", 0.75f, "islocal", true, "time", 0.1f, "easetype", iTween.EaseType.linear));
//		iTween.ScaleTo(obj, iTween.Hash("x", 1, "y", 1, "islocal", true, "time", 0.1f, "easetype", iTween.EaseType.linear, "delay", 0.1f));
//	}


	public static void LinearMoveTo(GameObject obj, Vector3 des,float time, float delay=0.0f, System.Action callBack = null, bool isLoopBack=false)
	{
		GetInstance().StartCoroutine(MoveToLinear (obj, des, time, delay, callBack, isLoopBack));
	}


	public static void ScaleTo(GameObject obj, Vector3 scale,float time, float delay=0.0f, System.Action callBack = null)
	{
		GetInstance().StartCoroutine(ScaleObjTo (obj, scale, time, delay, callBack));
	}
		


	/// <summary>
	/// Moves gameobject in a rectangle border with curved border.
	/// </summary>
	/// <param name="obj">Object.</param>
	/// <param name="center">Center.</param>
	/// <param name="width">Width.</param>
	/// <param name="height">Height.</param>
	/// <param name="circleRadius">curve radius.</param>
	/// <param name="speedAccelerate">Speed accelerate.</param>
	/// <param name="delay">Delay.</param>
	/// <param name="callBack">Call back.</param>
	public static void MoveCurveRect(GameObject obj, Vector3 center, float width, float height, float circleRadius, 
		float speedAccelerate = 1.0f, float angleAccelerate = 1.0f, float delay =0.0f, System.Action callBack = null, int startEdge = 0){
		GetInstance ().StartCoroutine (MoveRectCurve(obj, center, width, height, circleRadius,speedAccelerate,angleAccelerate, delay, callBack, startEdge));
	}

	public static void DelayInvokeMethod(System.Action<object[]> method, float delayTime, params object[] parameters){
		GetInstance ().StartCoroutine (InvokeMethodDelay (method, delayTime));
	}

	public static void FadeTo(float startAlpha, float desAlpha, GameObject obj, float time, float delay=0, System.Action callback=null, bool isCanvasObj = true){
		if (isCanvasObj)
			GetInstance ().StartCoroutine (FadeCanvasObjectTo (startAlpha, desAlpha, obj, time, delay, callback));
		else
			GetInstance ().StartCoroutine (Fade3dObjectTo (startAlpha, desAlpha, obj, time, delay, callback));
	}

	public static void MoveBySin(GameObject go, float yIncrease, float time, int direction =1, float k1=34, float k2=80,  float delay=0.0f, System.Action callBack = null){
		GetInstance().StartCoroutine(AnimMoveBySin(go, yIncrease, time, direction, k1, k2, delay, callBack));
	}

	#region corotines
	static IEnumerator MoveToCurve(GameObject obj, float desX, float a, float b, float c, float d, float e, float time, float delay=0.0f, System.Action<object> callBack = null )
	{
		Transform trans = obj.GetComponent<Transform>();
		float elapsedTime = 0;
		float originX = trans.localPosition.x;
		float newY;
		float newX;
		float distance = desX - originX;

		if (delay != 0.0f)
			yield return new WaitForSeconds (delay);

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			newX = originX + distance * (elapsedTime / time);
			newX /= 60.0f;
			newY = a + b * newX + c * newX * newX + d * newX * newX * newX;

			trans.localPosition = new Vector3 (newX*60.0f, newY*60.0f, 0);

			yield return null;
		}

		if (callBack != null)
			callBack(1);
	}

	static IEnumerator MoveToLinear(GameObject obj, Vector3 des,float time, float delay=0.0f, System.Action callBack = null , bool isLoopBack = false)
	{


		Transform trans = obj.transform;
		float elapsedTime = 0;
		float originX = trans.localPosition.x;
		float originY = trans.localPosition.y;
		float originZ = trans.localPosition.z;

		float newY, newZ, newX;
		float distanceX = des.x - originX;
		float distanceY = des.y - originY;
		float distanceZ = des.z - originZ;

		yield return new WaitForSeconds (delay);

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			float progress = elapsedTime / time; 
			newX = originX + distanceX * progress;
			newY = originY + distanceY * progress;
			newZ = originZ + distanceZ * progress;

			if (trans == null) {
				#if DEBUG
				Debug.Log("MoveToLinear game object destroyed!");
				#endif
				yield break;
			} else {
				trans.localPosition = new Vector3 (newX, newY, newZ);
			}

			yield return null;
		}

		if (trans != null) {
			trans.localPosition = des;

			if (isLoopBack) {
				GetInstance ().StartCoroutine (MoveToLinear (obj, new Vector3(originX, originY, originZ), time, 0, callBack));
			} else {
				if (callBack != null)
					callBack();
			}

		}
	}

	static IEnumerator ScaleObjTo(GameObject obj, Vector3 scale,float time, float delay=0.0f, System.Action callBack = null)
	{


		Transform trans = obj.transform;
		float elapsedTime = 0;
		float originX = trans.localScale.x;
		float originY = trans.localScale.y;
		float originZ = trans.localScale.z;

		float newY, newZ, newX;
		float distanceX = scale.x - originX;
		float distanceY = scale.y - originY;
		float distanceZ = scale.z - originZ;

		yield return new WaitForSeconds (delay);

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			float progress = elapsedTime / time; 
			newX = originX + distanceX * progress;
			newY = originY + distanceY * progress;
			newZ = originZ + distanceZ * progress;

			if (trans == null) {
				#if DEBUG
				Debug.Log("MoveToLinear game object destroyed!");
				#endif
				yield break;
			} else {
				trans.localScale = new Vector3 (newX, newY, newZ);
			}

			yield return null;
		}

		if (trans != null) {
			trans.localScale = scale;

			if (callBack != null)
				callBack();
		}
	}


	static IEnumerator InvokeMethodDelay(System.Action<object[]> method, float delayTime, params object[] parameters)
	{
		yield return new WaitForSeconds (delayTime);
		method (parameters);
		method = null;
	}

//	static IEnumerator FadeObject(GameObject obj, float target, float time, float delay =0.0f,  System.Action callBack = null){
//		Transform trans = obj.transform;
//
//		float startAlpha = obj.GetComponent<CanvasRenderer> ().GetAlpha;
//		CanvasRenderer[] rend = obj.GetComponentsInChildren<CanvasRenderer>();
//
//
//		float newAlpha;
//		float distanceX = target - startAlpha;
//
//		yield return new WaitForSeconds (delay);
//
//		float elapsedTime = 0;
//		while (elapsedTime < time)
//		{
//			elapsedTime += Time.deltaTime;
//			if (elapsedTime > time) {
//				elapsedTime = time;
//			}
//
//			float progress = elapsedTime / time; 
//			newAlpha = startAlpha + distanceX * progress;
//
//			if (trans == null) {
//				#if DEBUG
//				Debug.Log("FadeObject game object destroyed!");
//				#endif
//				yield break;
//			} else {
//				foreach (var item in rend)
//				{
//					if (item.GetAlpha()>newAlpha)
//						item.SetAlpha(newAlpha);
//				}
//			}
//
//			yield return null;
//		}
//
//
//		if (callBack != null)
//			callBack();
//	}

	// do not try to understand this
	// 	cWWc
	//  h  h
	//	h  h
	//  cWWc
	static IEnumerator MoveRectCurve(GameObject obj, Vector3 center, float width, float height, float circleRadius, 
		float speedAccelerate = 1.0f, float angleAccelerate = 1.0f,float delay =0.0f, System.Action callBack = null, int startAngle =0){

		float speed = 1.0f * speedAccelerate;
		Transform trans = obj.transform;

		yield return new WaitForSeconds (delay);

		switch(startAngle){
		case 0:
			{
				//start at top left
				trans.localPosition = new Vector3( center.x - width + circleRadius + speed/5, center.y + height, center.z);
				break;
			}
		case 2:
			{//top right
				trans.localPosition = new Vector3( center.x + width - circleRadius - speed/5, center.y - height, center.z);
				break;
			}
		}

		int quarterSpace = 1;
		float newX = trans.localPosition.x;
		float newY = trans.localPosition.y;
		float curX, curY;
		bool isInCircle = false;
				float angle = 0;
		bool x = true;
		int lastQuarter = 0;
		curX = trans.localPosition.x;
		curY = trans.localPosition.y;
		while (true) {
			//find quarter
			lastQuarter = quarterSpace;
			quarterSpace = 0;
			if (curX < center.x) {
				if (curY > center.y)
					quarterSpace = 1;
				else
					quarterSpace = 4;
			} else {
				if (curY > center.y)
					quarterSpace = 2;
				else
					quarterSpace = 3;
			}

			if (lastQuarter != quarterSpace) {
				switch (quarterSpace) {
				case 1:
					angle = 180;
					break;
				case 2:
					angle = 90;
					break;
				case 3:
					angle = 360;
					break;
				case 4:
					angle = 270;
					break;

				}
			}

			switch (quarterSpace) {
			case 1:
				{
					isInCircle = curX <= center.x - width + circleRadius && curY > center.y + height - circleRadius;
					if (isInCircle) {
						angle -= speed * angleAccelerate;
						newX = Mathf.Cos ((angle / 180) * 3.14f) * circleRadius + (center.x - width + circleRadius); //x=cos(angle)*R+a;
						newY = Mathf.Sin ((angle / 180) * 3.14f) * circleRadius + (center.y + height - circleRadius); //y=sin(angle)*R+b
					} else {
						if (1 >= Mathf.Abs (center.y + height - curY) && curX >= center.x - width + circleRadius)
							newX += speed;
						else
							newY += speed;
					}
				}
				break;
			case 2:
				{
					isInCircle = curX >= center.x + width - circleRadius && curY >= center.y + height - circleRadius;
					if (isInCircle) {
						angle -= speed * angleAccelerate;
						newX = Mathf.Cos ((angle / 180) * 3.14f) * circleRadius + (center.x + width - circleRadius); //x=cos(angle)*R+a;
						newY = Mathf.Sin ((angle / 180) * 3.14f) * circleRadius + (center.y + height - circleRadius); //y=sin(angle)*R+b
					} else {
						if (1 >= Mathf.Abs (center.y + height - curY) && curX <= center.x + width - circleRadius)
							newX += speed;
						else
							newY -= speed;
					}
				}
				break;
			case 3: 
				{
					isInCircle = curX >= center.x + width - circleRadius && curY <= center.y - height + circleRadius;
					//					Debug.Log ("abc " +isInCircle);
					if (isInCircle) {
						angle -= speed * angleAccelerate;
						newX = Mathf.Cos ((angle / 180) * 3.14f) * circleRadius + (center.x + width - circleRadius); //x=cos(angle)*R+a;
						newY = Mathf.Sin ((angle / 180) * 3.14f) * circleRadius + (center.y - height + circleRadius); ////y=sin(angle)*R+b
					} else {
						if (curY >= center.y - height && curX <= center.x + width - circleRadius)
							newX -= speed;
						else
							newY -= speed;
					}
				}
				break;
			case 4:
				{
					isInCircle = curX <= center.x - width + circleRadius && curY <= center.y - height + circleRadius;
					if (isInCircle) {
						angle -= speed * angleAccelerate;
						newX = Mathf.Cos ((angle / 180) * 3.14f) * circleRadius + (center.x - width + circleRadius); //x=cos(angle)*R+a;
						newY = Mathf.Sin ((angle / 180) * 3.14f) * circleRadius + (center.y - height + circleRadius); ////y=sin(angle)*R+b
					} else {
						if (1 >= Mathf.Abs (center.y - height - curY) && curX >= center.x - width + circleRadius)
							newX -= speed;
						else
							newY += speed;
					}
				}
				break;

			}

			bool inBorderX = Mathf.Abs (newX) <= width;
			bool inBorderY = Mathf.Abs (newY) <= height;

			curX = newX;
			curY = newY;

			if (!inBorderX)
				yield return null;
			if (inBorderY)
				yield return null;

			if (trans == null)
				yield break;
			trans.localPosition = new Vector3 (newX, newY, trans.localPosition.z);

			yield return null;
		}
	}

	static IEnumerator FadeCanvasObjectTo(float startAlpha, float desAlpha, GameObject obj, float time, float delay=0, System.Action callback=null )
	{

		CanvasRenderer[] rend = obj.GetComponentsInChildren<CanvasRenderer>();
		foreach (var item in rend) {
			item.SetAlpha (startAlpha);
		}

		float elapsedTime = 0;
		float distance = desAlpha - startAlpha;
		float currentAlpha = startAlpha;
		if (delay != 0.0f)
			yield return new WaitForSeconds (delay);

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			currentAlpha = startAlpha + distance * (elapsedTime / time);

			foreach (var item in rend) {
				if (item == null) // the game object was destroy
				{
					elapsedTime = float.MaxValue;
					break;
				}
				item.SetAlpha (currentAlpha);
			}
			yield return null;
		}

		if (callback != null)
			callback();
	}

	static IEnumerator Fade3dObjectTo(float startAlpha, float desAlpha, GameObject obj, float time, float delay=0, System.Action callback=null )
	{

//		MeshRenderer rend;
		Material mat = obj.GetComponent<Renderer>().material;
//		foreach (var item in rend) {
		mat.color = new Color (1, 1, 1, startAlpha); 
//		}


//		Debug.Log ("rend count " + rend.Length);
		float elapsedTime = 0;
		float distance = desAlpha - startAlpha;
		float currentAlpha = startAlpha;
		if (delay != 0.0f)
			yield return new WaitForSeconds (delay);

		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			currentAlpha = startAlpha + distance * (elapsedTime / time);

			mat.color = new Color (1, 1, 1, currentAlpha); 
//			mat.SetColor ("_Color", new Color (1, 1, 1, currentAlpha)); 
//			foreach (var item in rend) {
//				item.material.SetColor ("color", new Color (1, 1, 1, currentAlpha)); 
//			}
			yield return null;
		}

		if (callback != null)
			callback();
	}


	static IEnumerator AnimMoveBySin(GameObject obj, float yIncrease, float time, int direction =1,float k1 =34 , float k2=80, float delay=0.0f, System.Action callBack = null)
	{


		Transform trans = obj.transform;
		float elapsedTime = 0;
		float newY, newX;
		float distanceY = yIncrease;

		float originY = obj.transform.localPosition.y;
		float originX = obj.transform.localPosition.x;
		float originZ = obj.transform.localPosition.z;

		if (delay!=0)
			yield return new WaitForSeconds (delay);

		bool isFirstTime = true;
		while (elapsedTime < time)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime > time) {
				elapsedTime = time;
			}

			float progress = elapsedTime / time; 

			newY = originY + distanceY * progress;
			newX = direction*(k1) * Mathf.Sin (2 * (newY* 3.14f) / k2) + originX;

			if (isFirstTime) {
				float tmp = time / 30.0f;

				GameObjectUtils.LinearMoveTo (obj, new Vector3 (newX, newY, originZ), tmp);
				isFirstTime = false;
				yield return new WaitForSeconds(tmp);
			}

			if (trans == null) {
				#if DEBUG
				Debug.Log("AnimMoveBySin game object destroyed!");
				#endif
				yield break;
			} else {
				trans.localPosition = new Vector3 (newX, newY, originZ);
			}

			yield return null;
		}

//		if (trans != null) {
//			trans.localPosition = des;

//			if (isLoopBack) {
//				GetInstance ().StartCoroutine (MoveToLinear (obj, new Vector3(originX, originY, originZ), time, 0, callBack));
//			} else {
				if (callBack != null)
					callBack();
//			}

//		}
	}

	#endregion
}

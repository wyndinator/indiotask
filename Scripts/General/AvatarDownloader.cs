﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class AvatarDownloader : MonoBehaviour {

	const int MAX_DOWLOAD_RETRY = 1;

    public Image _avatar = null;
    private string _pathAvatar = "";

    static Dictionary<string, bool> _listDownloading = new Dictionary<string, bool>();

    public static Dictionary<string, bool> GetListDownloading()
    {
        return _listDownloading;
    }

    void Awake()
    {

    }

    void OnDestroy()
    {
        StopAllCoroutines();
        if (_listDownloading.ContainsKey(_pathAvatar) && _listDownloading[_pathAvatar] == true)
            _listDownloading.Remove(_pathAvatar);
    }

	public void Download(string path, bool doMaskAvatar = true, System.Action callback = null, bool setDefaultImage =true)
    {
//		path = "http://viss.gonct.info/viss/res/images/banner/nplay/avatar40.jpg";
//		path = "http://static.comicvine.com/uploads/original/11112/111122896/4626657-3259_goku.jpg";
//        path = "http://vignette4.wikia.nocookie.net/dragonballfanon/images/c/c0/DBZ_54.png/revision/latest/scale-to-width-down/280?cb=20130715205156&path-prefix=es";
        //path = "https://jeffreywong.files.wordpress.com/2011/06/fwef.jpg";
//        path = "https://s.ytimg.com/yts/img/avatar_720-vflYJnzBZ.png";

        if (path.Equals("") || !this.gameObject.activeInHierarchy)
        {
            Debug.LogWarning("Image download path is empty or inactive");
            return;
        }

        _pathAvatar = path;

        if (TextureCache.GetInstance().ContainSprite(path))
        {
            UpdateSprite(TextureCache.GetInstance().GetSprite(path));
            if (callback != null)
                callback();
        }
        // download
        else
		{
			if (setDefaultImage)
				UpdateSprite(TextureCache.GetInstance().GetDefaultSprite());
            StartCoroutine(StartDownloadAvatar(doMaskAvatar, callback));
        }

    }

    IEnumerator StartDownloadAvatar(bool doMaskAvatar = true, System.Action callback = null)
    {
//        if (isWait)
//            yield return new WaitForEndOfFrame();
        if (_listDownloading.ContainsKey(_pathAvatar))
        {

            while (_listDownloading[_pathAvatar] == true)
            {//downloading
                yield return new WaitForSeconds(0.3f);
            }

			if(TextureCache.GetInstance().ContainSprite(_pathAvatar)){
				UpdateSprite(TextureCache.GetInstance().GetSprite(_pathAvatar));
			}
            StopAllCoroutines();
        }
        else
        {
            _listDownloading.Add(_pathAvatar, true);
			WWW w= new WWW(_pathAvatar);

			int bytesDownloaded = 0;
			int downloadFailCounter = 0;

			while (bytesDownloaded == 0 || !string.IsNullOrEmpty (w.error)) {
				yield return w;

				while (!w.isDone) {
					yield return w;
				}

				bytesDownloaded = w.bytesDownloaded;

				if (w.bytesDownloaded==0 || !string.IsNullOrEmpty (w.error)){
					#if DEBUG
					Debug.Log("failed download avatar with path: "+_pathAvatar);
					#endif
					downloadFailCounter++;
					if (downloadFailCounter >= MAX_DOWLOAD_RETRY)
						break;
					w = new WWW(_pathAvatar);
				}
			}

			if (downloadFailCounter >= MAX_DOWLOAD_RETRY)
				TextureCache.GetInstance().AddSprite(null, _pathAvatar, doMaskAvatar);
			else
				TextureCache.GetInstance().AddSprite(w.texture, _pathAvatar, doMaskAvatar);

            if (_listDownloading.Count > 100)
            {
                foreach (var item in _listDownloading)
                {
                    if (!item.Value)
                    {
                        _listDownloading.Remove(item.Key);
                        break;
                    }
                }
            }

            UpdateSprite(TextureCache.GetInstance().GetSprite(_pathAvatar));
            _listDownloading[_pathAvatar] = false;
            StopAllCoroutines();
        }

        if (callback != null)
            callback();
    }

    IEnumerator WaitForTimeOut(bool isWait)
    {
        yield return new WaitForSeconds(15.0f);
        StopAllCoroutines();
		_listDownloading.Remove(_pathAvatar);
        StartCoroutine(StartDownloadAvatar(isWait));
    }

    void UpdateTexture( Texture2D text)
    {
        _avatar.sprite = Sprite.Create(text, new Rect(0, 0, text.width, text.height), new Vector2(0.5f, 0.5f));
    }

    void UpdateSprite(Sprite sprite)
    {
        _avatar.sprite = sprite;
    }


	public void DownloadWithFbID(string id){
//		Debug.Log ("id " + id);
		StartCoroutine (DownloadFbID (id));
	}

	IEnumerator DownloadFbID(string id){
		WWW url = new WWW("https" + "://graph.facebook.com/" + id + "/picture?type=small"); 
		_pathAvatar = id;
		Texture2D textFb2 = new Texture2D(128, 128, TextureFormat.DXT1, false); //TextureFormat must be DXT5
		yield return url;
		textFb2 = url.texture;
		TextureCache.GetInstance().AddSprite(url.texture, _pathAvatar, true);
		UpdateSprite(TextureCache.GetInstance().GetSprite(_pathAvatar));
//		UpdateTexture (textFb2);
	}
}

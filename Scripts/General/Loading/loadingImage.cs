﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class loadingImage : MonoBehaviour {

	[SerializeField]
    private Image _image;

	float _rotateZ;
    void Awake()
    {
//        var allImage = gameObject.GetComponentsInChildren<Image>();
//        for (int i = 0; i < allImage.Length; i++)
//        {
//            if (allImage[i].name == "imageLoading")
//            {
//                _image = allImage[i];
//
//            }
//        }
    }
    // Use this for initialization
    void Start()
    {
		_rotateZ = 0;
//		iTween.RotateBy(_image.gameObject, iTween.Hash("time", 1, "amount", new Vector3(0, 0, 6), "looptype", iTween.LoopType.pingPong));
    }

	public void IsShow(bool isShow){
		gameObject.SetActive (isShow);
	}

//	public void Show(){
//		gameObject.SetActive (true);
//
//	}


    // Update is called once per frame
    void Update()
    {
		_rotateZ += 12;
		if (_rotateZ > 99999)
			_rotateZ = -99999;
		_image.transform.localEulerAngles = new Vector3 (0, 0, -_rotateZ);
    }
}

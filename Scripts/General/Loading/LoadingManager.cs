﻿using UnityEngine;
using System.Collections;

public class LoadingManager : MonoBehaviour {

    static LoadingManager instance;
    public loadingImage loadding;
	System.Action _timeOutCallBack;
    // Use this for initialization
    public static LoadingManager getInstance()
    {
        if (instance == null)
        {
            GameObject gb = new GameObject("LoadingManager");
            DontDestroyOnLoad(gb);
			gb.transform.SetParent (GameObject.Find ("Canvas").transform);
			gb.transform.localScale = Vector3.one;
			gb.transform.localPosition = Vector3.zero;
            gb.AddComponent<LoadingManager>();
			instance = gb.GetComponent<LoadingManager>();
			instance.Init();
        }
        return instance;
    }

    void Start()
    {

    }

    void isShow(bool isShow)
    {
        if (loadding == null)
            Init();
        else
        {
            loadding.IsShow(isShow);
        }
    }

	public void Show(float time = 10.0f, System.Action timeOutCallBack = null){
		loadding.IsShow (true);
		_timeOutCallBack = timeOutCallBack;
		StartCoroutine ("CountDown", time);
	}

	IEnumerator CountDown(float time){
		yield return new WaitForSecondsRealtime (time);

		if (_timeOutCallBack != null)
			_timeOutCallBack ();
		loadding.IsShow (false);
	}

	public void Hide(){
		loadding.IsShow (false);
		StopCoroutine ("CountDown");
	}


    public void Init()
    {
        Object prefab = Resources.Load("LoadingPanel");
        GameObject clone = Instantiate(prefab) as GameObject;
//		clone.GetComponent<
		clone.transform.SetParent (gameObject.transform);
		clone.transform.localScale = Vector3.one;
        clone.transform.localPosition = new Vector3(0, 0, 0);
		clone.SetActive (true);
        loadingImage myComponent = clone.GetComponent(typeof(loadingImage)) as loadingImage;
        loadding = myComponent;
        //listDialog.Add(myComponent);
    }
}

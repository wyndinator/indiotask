﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class LanguageController : MonoBehaviour {
	LanguageManager _instanceLang;

	static LanguageController _instance;
	public static LanguageController GetInstance(){
		return _instance;
	}

	// Use this for initialization
	void Start () {
		_instance = this;
		_instanceLang = SmartLocalization.LanguageManager.Instance;
//		_instanceLang.
		StartCoroutine("DelayLoadLanguage");
	}

	IEnumerator DelayLoadLanguage(){
		yield return new WaitForSeconds(1.0f);
		_instanceLang.OnChangeLanguage += OnChangeLanguage;
		_instanceLang.ChangeLanguage ("vi");
	}

	void OnChangeLanguage(LanguageManager thisManger){
		Debug.Log ("fdsf");
	}

	public string Translate(string key){
		return _instanceLang.GetTextValue(key);
	}
}



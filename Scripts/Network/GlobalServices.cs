﻿using UnityEngine;
using System.Collections;

public class GlobalServices  {
	public static void RegisterAccount(string fbToken,  System.Action<bool, ServerResponseModel> callback){
		RegisterModel md= new RegisterModel ();
		md.FbToken = fbToken;
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.Register, new ServerRegisterResponseModel(),callback);
	}

	public static void LoginToServer(System.Action<bool, ServerResponseModel> callback){
		LoginModel md= new LoginModel ();
		md.FacebookID = UserController.GetInstance().UserModel.FacebookID;
		md.Token = UserController.GetInstance().UserModel.UserServerToken;
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.Login, new ServerRegisterResponseModel(),callback);
	}


	  public static void PostQuiz(System.Action<bool, ServerResponseModel> callback,string quizTitle,string quizContent, 
		string op1, string op2, string op3, string op4, string quizExplain, int quizAnswer, int quiztype =0){

		PostQuizRequestModel md= new PostQuizRequestModel (
			UserController.GetInstance().UserModel.UserServerToken, quizTitle, quizContent, op1, op2, op3, op4, quizExplain, quizAnswer, quiztype);

		if (!md.IsValidQuiz ()) {
			callback (false, new ServerResponseModel ());
			return;
		}

		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.PostQuiz, new ServerRegisterResponseModel(),callback);
	}

	public static void GetQuizFromSever(int quizIndex, System.Action<bool, ServerResponseModel> callback){
		
		GetQuizRequestModel md = new GetQuizRequestModel (UserController.GetInstance ().UserModel.UserServerToken, quizIndex);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.GetQuiz, new GetQuizResponseModel(),callback);
	}

	public static void AnsQuiz(string quizID, int quizAnsIndex, System.Action<bool, ServerResponseModel> callback){
		AnsQuizRequestModel md = new AnsQuizRequestModel (UserController.GetInstance ().UserModel.UserServerToken, quizID, quizAnsIndex);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.AnsQuiz, new ServerResponseModel(),callback);
	}

	public static void GetQuizStatistic(string quizID, System.Action<bool, ServerResponseModel> callback){
		GetQuizStatisticRequestModel md = new GetQuizStatisticRequestModel (UserController.GetInstance ().UserModel.UserServerToken, quizID);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.GetStats, new QuizStatisticResponseModel(),callback);
	}

	public static void LikeQuiz(string quizID, System.Action<bool, ServerResponseModel> callback){
		LikeQuizRequestModel md = new LikeQuizRequestModel (UserController.GetInstance ().UserModel.UserServerToken, quizID);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.LikeQuiz, new ServerResponseModel(),callback);
	}

	public static void ReportQuiz(string quizID, System.Action<bool, ServerResponseModel> callback){
		ReportQuizRequestModel md = new ReportQuizRequestModel (UserController.GetInstance ().UserModel.UserServerToken, quizID);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.ReportQuiz, new ServerResponseModel(),callback);
	}

	public static void UpdateOneSignal(string oneSignalID, System.Action<bool, ServerResponseModel> callback){
		UpdateOneSignalModel md = new UpdateOneSignalModel (UserController.GetInstance ().UserModel.UserServerToken, oneSignalID);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.UpdateOneSignal, new ServerResponseModel(),callback);
	}

	public static void GetConfigServer(System.Action<bool, ServerResponseModel> callback){
		GetConfigRequestModel md = new GetConfigRequestModel (UserController.GetInstance ().UserModel.UserServerToken);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.GetGameConfig, new GetConfigResponseModel(),callback);
	}

	public static void GetGameNews(System.Action<bool, ServerResponseModel> callback){
		GetGameNewsRequestModel md = new GetGameNewsRequestModel (UserController.GetInstance ().UserModel.UserServerToken);
		NetworkController.GetInstance ().SendMessageToServer (md.ToJson (), GameCommand.GetGameNews, new GetGameNewsResponseModel(),callback);
	}
}

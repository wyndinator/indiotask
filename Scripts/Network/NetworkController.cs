﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class NetworkController : MonoBehaviour {


	static NetworkController _instance;
	public static NetworkController GetInstance(){
		if (_instance == null) {
			GameObject go = new GameObject ("NetworkController");
			DontDestroyOnLoad (go);
			go.AddComponent<NetworkController> ();
			_instance = go.GetComponent<NetworkController> ();
		}
		return _instance;
	}
		

	public void SendMessageToServer(string jsonMess, string command,  ServerResponseModel model, System.Action<bool,ServerResponseModel> callBack = null){

		StartCoroutine (SendMessageAsync (jsonMess, command, model, callBack));

	}

	IEnumerator SendMessageAsync(string jsonMess, string command, ServerResponseModel model, System.Action<bool,ServerResponseModel> callBack = null){

		WWWForm w = new WWWForm();
		w.AddField("data", jsonMess);
		Utility.DebugLog ("send command: "+command+" data json: " + jsonMess);
		WWW www = new WWW("http://52.35.67.156:8080"+command, w);

		yield return www;

		if (string.IsNullOrEmpty (www.error)) {
			string json =www.text;
			Utility.DebugLog ("receive messsage json: "+ json);
			model.UpdateFromJson (json);

			if (model.IsSuccess()) {

				if (callBack!=null)
					callBack (true, model);
			} else {

				Utility.DebugError ("error receive message "+command +"; error: "+model.Message + "; code: "+model.Code + "response json " +www.text);
				if (callBack!=null)
					callBack (false, model);
			}
		} else {
			if (callBack!=null)
				callBack (false, model);
			Utility.DebugError ("error receive message "+command +"; error: "+www.error );
		}
	}
}

﻿using System;


public class SocialUserInfo
{
	string _email;
	string _userName;
    string _userId;

	string _token;
	public string Token{
		get{ return _token;}
		set{_token = value;}
	}
	public SocialUserInfo(string email, string userName){
		_email = email;
		_userName = userName;
	}
    public SocialUserInfo(string email, string userName,string userId)
    {
        _email = email;
        _userName = userName;
        _userId = userId;
    }


}

public class SocialResult{
	string _message;
	bool _isSuccess;

	public string Message {
		get{ return _message;}
		set{_message = value;}
	}

	public bool IsSuccess {
		get{ return _isSuccess;}
		set{_isSuccess = value;}
	}

	public SocialResult(bool isSuccess, string message){
		_message = message;
		_isSuccess = isSuccess;
	}
}

public class SocialLoginResult:SocialResult{
	bool _isUserCancel;


	public bool IsUserCancel {
		get{ return _isUserCancel;}
		set{_isUserCancel = value;}
	}

	public SocialLoginResult(bool isSuccess, string message, bool isCancel) : base(isSuccess, message){
		base.IsSuccess = isSuccess;
		base.Message = message;
		_isUserCancel = isCancel;
	}
}

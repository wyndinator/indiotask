﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using Facebook.MiniJSON;
public class FacebookImpl : SocialImpl
{
//    System.Action<SocialResult> callbackInit;
//    System.Action<SocialResult> callbackLogin;

    public override void Initialize(System.Action<SocialResult> callback){
		if (!FB.IsInitialized) {
			FB.Init (delegate {
				if (FB.IsInitialized){
					FB.ActivateApp ();
					callback (new SocialResult (true, " initialize sucess"));
				}else{
					callback (new SocialResult (false, " initialize failed"));
				}
			});
//            callbackInit = callback;
		} else {
			FB.ActivateApp();
			callback (new SocialResult (true, "test initialize sucess"));
		}
	}

    public override void Login(System.Collections.Generic.List<string> userPermissions, System.Action<SocialResult> callback){
		//callback(new SocialResult(true, "test login sucess"));
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, delegate(ILoginResult result) {
			if (string.IsNullOrEmpty(result.Error)){
				if (result.Cancelled){
					callback(new SocialLoginResult(false, "user cancel process", true));
				}else{
                    //AuthCallback(result);
                    callback(new SocialLoginResult(true, "login success", false));
				}
			}
			else{
				callback(new SocialLoginResult(false, result.Error, false));
			}
        });
    }
    public override void GetUserInfo(System.Action<SocialResult> callback) {
        FB.API("/me", HttpMethod.GET, graphResult =>
        {
            if (string.IsNullOrEmpty(graphResult.Error) == false)
            {
//                Debug.Log("could not get email address");
                callback(new SocialResult(false, graphResult.Error));
                return;
            }
            Debug.Log("lay email");
            string userid = graphResult.ResultDictionary["id"] as string;
				string email = "";
//            string email = graphResult.ResultDictionary["email"] as string;
            string name = graphResult.ResultDictionary["name"] as string;
				string token = AccessToken.CurrentAccessToken.TokenString;
            //string last_name = graphResult.ResultDictionary["last_name"] as string;
             Debug.Log(email + " name: " + name+"UserId " +userid);
            SocialUserInfo tmp = new SocialUserInfo(email,name, userid);
				tmp.Token = token;
            SocialManager.GetInstance().UserInfoModel = tmp;
            callback(new SocialResult(true, "test get user info sucess"));
        });
    }
	public override bool IsInitialized(){
		return true;
	}
	private void AuthCallback(ILoginResult result)
	{
		if (FB.IsLoggedIn)
		{
			string allData = "";
			string userId;
			// AccessToken class will have session details
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			// Print current access token's User ID
			Debug.Log(aToken.UserId);
			userId = aToken.UserId;
			foreach (string perm in aToken.Permissions)
			{
				Debug.Log(perm);
				//allData+=perm+"  ";
			}
			FB.API("/me?fields=first_name,last_name,email", HttpMethod.GET, graphResult =>
				{
					if (string.IsNullOrEmpty(graphResult.Error) == false)
					{
						Debug.Log("could not get email address");
						return;
					}
					Debug.Log("lay email");
					string email = graphResult.ResultDictionary["email"] as string;
					string first_name = graphResult.ResultDictionary["first_name"] as string;
					string last_name = graphResult.ResultDictionary["last_name"] as string;
					Debug.Log(email + " name: " + first_name + " " + last_name);
					allData += first_name + " " + last_name;
					SocialUserInfo tmp = new SocialUserInfo(email,allData,userId);
					SocialManager.GetInstance().UserInfoModel = tmp;
				});

		}
		else
		{
			Debug.Log("User cancelled login");
		}
	}
}


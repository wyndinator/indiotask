﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SocialPlatform{
	FACEBOOK,
	GUEST,
	NONE
}

public class SocialManager {

	SocialPlatform _currentPlatform;
	SocialImpl _socialImpl;

	SocialUserInfo _model;
	public SocialUserInfo UserInfoModel{
		get{ return _model;}
		set{_model = value;}
	}

	static SocialManager _instance;
	public static SocialManager GetInstance(){
		if (_instance == null) {
			_instance = new SocialManager ();
		}

		return _instance;
	}

	public void Initilalize(SocialPlatform platform, System.Action<SocialResult> callback){
		switch (platform) {
		case SocialPlatform.FACEBOOK:
			_socialImpl = new  FacebookImpl ();
			break;
//		default:
//			_socialImpl = new FacebookImpl ();
		}

		_socialImpl.Initialize (callback);
	}

	public void Login(List<string> userPermissions, System.Action<SocialResult> callback){
		if (_socialImpl.IsInitialized ()) {
			_socialImpl.Login (userPermissions,callback);
		} else {
			callback(new SocialResult(false, "not initialized"));
		}
	}

	public void GetUserInfo(System.Action<SocialResult> callback){
		_socialImpl.GetUserInfo (callback);
	}
}

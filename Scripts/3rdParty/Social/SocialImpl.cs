﻿using System;


public abstract class SocialImpl
{
	public abstract void Initialize(System.Action<SocialResult> callback);
	public abstract bool IsInitialized();
	public abstract void Login(System.Collections.Generic.List<string> userPermissions, System.Action<SocialResult> callback);
	public abstract void GetUserInfo(System.Action<SocialResult> callback);
}

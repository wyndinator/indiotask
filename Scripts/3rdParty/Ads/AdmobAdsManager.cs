﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdmobAdsManager : AdsTemplate {

	#region video
	public override void Init(){

	}

	public override bool IsRewardedVideoAvailable(){
		return true;
	}


	public override void ShowRewardedVideo(System.Action<bool> finishCallBack){
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-3383686483162737/3376172000";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_AD_UNIT_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		_finishCallBack = finishCallBack;

		RewardBasedVideoAd rewardBasedVideo = RewardBasedVideoAd.Instance;

		if (rewardBasedVideo.IsLoaded ()) {
		rewardBasedVideo.Show ();
		return;
		}

		AdRequest request = new AdRequest.Builder().Build();
		_finishCallBack = finishCallBack;
		rewardBasedVideo.LoadAd(request, adUnitId);
		Utility.DebugLog ("loading rewarded");
		rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
		rewardBasedVideo.OnAdClosed += OnRewardedVideoClosed;
		}

		void HandleRewardBasedVideoLoaded(object a, System.EventArgs b){
		Utility.DebugLog ("loaded rewarded");
		RewardBasedVideoAd.Instance.Show ();
		}

		void OnRewardedVideoClosed(object a, System.EventArgs b){
		_finishCallBack (true);
		}
		#endregion

		#region banner

		public override bool IsBannerAdsAvailable(){
		return true;
		}

		public override bool IsBannerAdsShowing(){
		return _isBannerShowing;
		}

		BannerView bannerView;
		public override void ShowBannerAds(BannerAdsPos position = BannerAdsPos.TOP){
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-3383686483162737/7945972406";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();

		// Load the banner with the request.
		_isBannerShowing = false;
		bannerView.LoadAd(request);

		// Called when an ad request has successfully loaded.
		bannerView.OnAdLoaded += HandleOnAdLoaded;

		// Called when an ad request failed to load.
		bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;
		bannerView.OnAdOpening += HandleOnAdFailedToLoad;
		//		bannerView.on

		//		// Called when an ad is clicked.
		//		bannerView.OnAdOpened += HandleOnAdOpened;

		// Called when the user returned from the app after an ad click.
		bannerView.OnAdClosed += HandleOnAdClosed;
		// Called when the ad click caused the user to leave the application.
		bannerView.OnAdLeavingApplication += HandleOnAdLeavingApplication;
		}

		public override void HideBannerAds(){
		bannerView.Hide ();
		}
		void HandleOnAdLoaded(object sender, EventArgs args){
		Utility.DebugLog ("success to load");
		bannerView.Show ();
		}

		void HandleOnAdFailedToLoad(object sender, EventArgs args){
		Utility.DebugLog ("fail to load");
		_isBannerShowing = false;
		}

		void HandleOnAdOpened(object sender, EventArgs args){

		Utility.DebugLog ("ad open");
		}

		void HandleOnAdClosed(object sender, EventArgs args){

		Utility.DebugLog ("ad closed");
		}

		void HandleOnAdLeavingApplication(object sender, EventArgs args){

		Utility.DebugLog ("Ad leaving");
		}

		void HandleOnAdOpen(object sender, EventArgs args){
		_isBannerShowing = true;
		Utility.DebugLog ("banner ad opening");
		}

		#endregion

		#region interstitial

		InterstitialAd _interstitial;

		public override bool IsInterstitialAdsAvailable(){
		return true;
		}

		public override void ShowInterstitialAds(){
		if (_interstitial!=null && _interstitial.IsLoaded ())
			_interstitial.Show ();
		LoadInterstitialAds ();
		}

		public override void LoadInterstitialAds(){
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-4096026650414979/1648963841";
		#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		_interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		_interstitial.LoadAd(request);
		_interstitial.OnAdLoaded += HandleOnInterstitialAdLoaded;
		}

		void HandleOnInterstitialAdLoaded(object sender, EventArgs args){
		Utility.DebugLog ("success to load");
		}


		void HandleOnInterstitialAdFailedToLoad(object sender, EventArgs args){
		Utility.DebugLog ("fail to load");
		}

		void HandleOnInterstitialAdOpened(object sender, EventArgs args){

		Utility.DebugLog ("ad open");
		}

		void HandleOnInterstitialAdClosed(object sender, EventArgs args){

		Utility.DebugLog ("ad closed");
		}

		void HandleOnInterstitialAdLeavingApplication(object sender, EventArgs args){

		Utility.DebugLog ("Ad leaving");
		}
		#endregion
		}

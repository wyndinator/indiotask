﻿using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.Advertisements;
using System.Collections;
using System.Collections.Generic;
public class AdsManager : MonoBehaviour {

	public enum AdProvider
	{
		NONE,
		UNITY,
		CHARTBOOST,
		AUTO,
		ADMOB
	}

	public enum AdShowAction
	{
		BACK_MENU,
		NEXT_QUIZ,
		ANS_QUIZ
	}


    static AdsManager _instance = null;
    public static AdsManager GetInstancce()
    {

        return _instance;
    }

	Dictionary<AdShowAction, int> _listAdShowAcitonRandom;
    Dictionary<AdProvider, AdsTemplate> _listAds;
    void Start()
    {
        _instance = this;

		_listAds = new Dictionary<AdProvider, AdsTemplate>();
		_listAds.Add(AdProvider.ADMOB, new AdmobAdsManager());
    }

	public void InitAdsActionRandom(AdConfigModel model){
		_listAdShowAcitonRandom = new Dictionary<AdShowAction, int> ();
		_listAdShowAcitonRandom.Add (AdShowAction.ANS_QUIZ, model.passFive);
		_listAdShowAcitonRandom.Add (AdShowAction.BACK_MENU, model.backToMenu);
		_listAdShowAcitonRandom.Add (AdShowAction.NEXT_QUIZ, model.nextThree);
	}

	#region video ads
    public void ShowVideoRewardAd(System.Action<bool> callback=null)
	{
		ShowVideoAd ();
    }

    AdProvider GetAvailableVideoProvider()
    {
        foreach (var item in _listAds)
        {
            if (item.Value.IsRewardedVideoAvailable())
            {
                return item.Key;
            }
        }

        return AdProvider.NONE;
    }

    void ShowVideoAd()
    {
		foreach (var item in _listAds.Values) {
			if (item.IsRewardedVideoAvailable ())
				item.ShowRewardedVideo (OnFinishViewVideoAd);
		}
//        if (_currentAvaiableProvider != AdProvider.NONE)
//        {
//			_listAds[_currentAvaiableProvider].ShowRewardedVideo(OnFinishViewVideoAd);
//        }
//        else
//        {
//            ShowCanNotViewAdsDialog();
//        }
    }

    public void ShowCanNotViewAdsDialog()
    {
//        WaittingLayerScript.GetInstance().Dismiss();
//		DialogManager.GetInstance().AddDialog(LanguageManager.Instance.GetTextValue(LanguageKey.Dialog_OopsTitle), 
//			LanguageManager.Instance.GetTextValue(LanguageKey.Dialog_CantViewAds), (int)TagDialog.ERROR, null, DialogManager.TypeDialog.ONE_BUTTON);
    }

	void OnFinishViewVideoAd(bool isSuccess)
    {
		if (isSuccess) {
//			GlobalService.GetIntansce().OnSysRequestVideoIncent(_currentToken);
//			WaittingLayerScript.GetInstance().Dismiss();
		} 
		else {      
//			WaittingLayerScript.GetInstance().Dismiss();
//			DialogManager.GetInstance().AddDialog(LanguageManager.Instance.GetTextValue(LanguageKey.Dialog_NoticeTitle),
//				LanguageManager.Instance.GetTextValue(LanguageKey.Dialog_CancelVideo), 
//				(int)TagDialog.ERROR, null, DialogManager.TypeDialog.ONE_BUTTON);
		}
    }

    public bool IsVideoAdsAvailable()
    {
        foreach (var item in _listAds)
        {
            if (item.Value.IsRewardedVideoAvailable())
            {
                return true;
            }
        }

        return false;
    }
	#endregion video ads

	#region interstitial ads

	public void ShowInterstitialAds(AdShowAction act, AdProvider provider = AdProvider.AUTO){
		if (Random.Range (0, 100) > _listAdShowAcitonRandom [act]) {
			Utility.DebugLog ("Random Not showing ads");
			return;
		}
		
		if (provider == AdProvider.AUTO) {
			foreach (var item in _listAds.Values) {
				item.ShowInterstitialAds ();
			}
		} else {
			_listAds [provider].ShowBannerAds ();
		}

	}

	public void LoadInterstitialAds( AdProvider provider = AdProvider.AUTO){
		if (provider == AdProvider.AUTO) {
			foreach (var item in _listAds.Values) {
				item.LoadInterstitialAds ();
			}
		} else {
			_listAds [provider].LoadInterstitialAds ();
		}
	}
	#endregion 

	#region banner ads

	public bool CanShowBanner(){
		foreach (var item in _listAds.Values) {
			if (item.IsBannerAdsAvailable ())
				return true;
		}

		return false;
	}

	public void ShowBannerAds(AdProvider provider = AdProvider.AUTO){
		if (provider == AdProvider.AUTO) {
			foreach (var item in _listAds.Values) {
				if (item.IsBannerAdsAvailable ())
					item.ShowBannerAds ();
			}
		} else {
			_listAds [provider].ShowBannerAds ();
		}

	}

	#endregion banner ads
}

﻿using UnityEngine;
using System.Collections;

public abstract class AdsTemplate {

	protected AdsTemplate()
	{
		Init();
	}

	protected System.Action<bool> _finishCallBack;

	public abstract void Init();

	public abstract bool IsRewardedVideoAvailable();
	public abstract void ShowRewardedVideo(System.Action<bool> finishCallBack);


	public enum BannerAdsPos{
		TOP,
		BOTTOM
	}
	protected bool _isBannerShowing = false;
	public abstract bool IsBannerAdsAvailable();
	public abstract bool IsBannerAdsShowing();
	public abstract void ShowBannerAds(BannerAdsPos position = BannerAdsPos.TOP);
	public abstract void HideBannerAds();

	public abstract bool IsInterstitialAdsAvailable();
	public abstract void ShowInterstitialAds();
	public abstract void LoadInterstitialAds();
}

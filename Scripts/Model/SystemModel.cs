﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;




public class UserModel : BaseModel {
	string token; //server token
	public string UserServerToken
	{
		get { return token; }
		set { token = value; }
	}


	string fullName;
	public string FullName
	{
		get { return fullName; }
		set { fullName = value; }
	}

	string facebookID;
	public string FacebookID
	{
		get { return facebookID; }
		set { facebookID = value; }
	}

	int configVersion;
	public int ConfigVersion
	{
		get { return configVersion; }
		set { configVersion = value; }
	}
}

public class QuizStatisticModel:BaseModel{
	int option0;
	int option1;
	int option2;
	int option3;


	public int Option0
	{
		get { return option0; }
		set { option0 = value; }
	}

	public int Option1
	{
		get { return option1; }
		set { option1 = value; }
	}

	public int Option2
	{
		get { return option2; }
		set { option2 = value; }
	}

	public int Option3
	{
		get { return option3; }
		set { option3 = value; }
	}


//	public QuizStatisticModel (int op0, int op1, int op2, int op3){
//		option0 = op0;
//		option1 = op1;
//		option2 = op2;
//		option3 = op3;
//	}
}

//public class QuizModel: BaseModel{
//	string id; //server token
//	public string UserServerToken
//	{
//		get { return token; }
//		set { token = value; }
//	}
//
//	string id; //server token
//	public string UserServerToken
//	{
//		get { return token; }
//		set { token = value; }
//	}
//
//	string id; //server token
//	public string UserServerToken
//	{
//		get { return token; }
//		set { token = value; }
//	}
//}

public class QuizModel : BaseModel{
	string id;
	string url;
	int type;
	string content;
	string explane;
	QuizAnsModel option;
	int answer;
	string author;
	string authorFacebookID;
	int like;

	public List<string> GetListAns(){
		return option.ToList ();
	}

	public string Author
	{
		get { return author; }
		set { author = value; }
	}

	public string Explain
	{
		get { return explane; }
		set { explane = value; }
	}

	public int Answer
	{
		get { return answer; }
		set { answer = value; }
	}

	public string ID
	{
		get { return id; }
		set { id = value; }
	}

	public string ImageUrl
	{
		get { return url; }
		set { url = value; }
	}

	public string Content
	{
		get { return content; }
		set { content = value; }
	}

	public int Like
	{
		get { return like; }
		set { like = value; }
	}

	public bool HasImage(){
		return url.Length>0;
	}

	public string AuthorFacebookID
	{
		get { return authorFacebookID; }
		set { authorFacebookID = value; }
	}
}

public class QuizAnsModel : BaseModel{
	string option0;
	string option1;
	string option2;
	string option3;

	public List<string> ToList(){
		List<string> result = new List<string> ();

		if (!string.IsNullOrEmpty(option0))
			result.Add (option0);

		if (!string.IsNullOrEmpty(option1))
			result.Add (option1);

		if (!string.IsNullOrEmpty(option2))
			result.Add (option2);

		if (!string.IsNullOrEmpty(option3))
			result.Add (option3);

		return result;
	}
}

public class HelpConfigModel : BaseModel{
	public string urlImage0, urlImage1, urlImage2, urlImage3;
	public void SaveToLocal(){
		PlayerPrefs.SetString (GameLocalDataKey.HelpUrlImage0, urlImage0);
		PlayerPrefs.SetString (GameLocalDataKey.HelpUrlImage1, urlImage1);
		PlayerPrefs.SetString (GameLocalDataKey.HelpUrlImage2, urlImage2);
		PlayerPrefs.SetString (GameLocalDataKey.HelpUrlImage3, urlImage3);
	}

	public void LoadFromLocal(){
		PlayerPrefs.GetString (GameLocalDataKey.HelpUrlImage0, urlImage0);
		PlayerPrefs.GetString (GameLocalDataKey.HelpUrlImage1, urlImage1);
		PlayerPrefs.GetString (GameLocalDataKey.HelpUrlImage2, urlImage2);
		PlayerPrefs.GetString (GameLocalDataKey.HelpUrlImage3, urlImage3);
	}
}

public class AboutConfigModel : BaseModel{
	public string character, fanpage, copyright;
	public void SaveToLocal(){
		PlayerPrefs.SetString (GameLocalDataKey.AboutConfigCharacter, character);
		PlayerPrefs.SetString (GameLocalDataKey.AboutConfigFanpage, fanpage);
		PlayerPrefs.SetString (GameLocalDataKey.AboutConfigCopyright, copyright);
	}

	public void LoadFromLocal(){
		PlayerPrefs.GetString (GameLocalDataKey.AboutConfigCharacter, "");
		PlayerPrefs.GetString (GameLocalDataKey.AboutConfigFanpage, "");
		PlayerPrefs.GetString (GameLocalDataKey.AboutConfigCopyright, "");
	}
}

public class AdConfigModel : BaseModel{
	public int nextThree, backToMenu, passFive;

	public void SaveToLocal(){
		PlayerPrefs.SetInt (GameLocalDataKey.AdsConfigPassFive, passFive);
		PlayerPrefs.SetInt (GameLocalDataKey.AdsConfigNextThree, nextThree);
		PlayerPrefs.SetInt (GameLocalDataKey.AdsConfigBackToMenu, backToMenu);
	}

	public void LoadFromLocal(){
		PlayerPrefs.GetInt (GameLocalDataKey.AdsConfigPassFive, 0);
		PlayerPrefs.GetInt (GameLocalDataKey.AdsConfigNextThree, 0);
		PlayerPrefs.GetInt (GameLocalDataKey.AdsConfigBackToMenu, 0);
	}
}

public class SloganConfigModel : BaseModel{
	public List<string> _listSlogan= new List<string>();

	public void SaveToLocal(){
		for (int i = 0; i < _listSlogan.Count; i++) {
			PlayerPrefs.SetString (GameLocalDataKey.SloganConfigItem + i.ToString (), _listSlogan [i]);
		}
		PlayerPrefs.SetInt (GameLocalDataKey.TotalSloganItemCount, _listSlogan.Count);
	}

	public void LoadFromLocal(){
		int count = PlayerPrefs.GetInt (GameLocalDataKey.TotalSloganItemCount, 0);
		for (int i = 0; i < count; i++) {
			_listSlogan.Add (PlayerPrefs.GetString (GameLocalDataKey.SloganConfigItem + i.ToString (), ""));
		}
	}

	public void UpdateFromList(List<string> listNewSlogan){
		_listSlogan.Clear ();
		_listSlogan.AddRange (listNewSlogan);
	}
}

public class ConfigModel : BaseModel{
	public int configVersion;
	public int immediate;
	public float version;
	//	public HelpConfigModel help;
	public List<string> help;
	public List<string> slogan;
	public AdConfigModel ads;
	public AboutConfigModel about;
	public string policy;
}

public class GameNewsModel : BaseModel{
	string id, content, url;

	public string Id
	{
		get { return id; }
		set { id = value; }
	}

	public string Content
	{
		get { return content; }
		set { content = value; }
	}

	public string Url
	{
		get { return url; }
		set { url = value; }
	}
}
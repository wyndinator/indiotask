﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using SimpleJSON;
using System.Runtime.Remoting;
using System;
//using NiceJson;

//[System.Serializable]
public class BaseModel  {

	public string ToJson(){
		BindingFlags bindingFlags = BindingFlags.Public |
			BindingFlags.NonPublic |
			BindingFlags.Instance |
			BindingFlags.Static;

		JSONObject o = new JSONObject ();
		foreach (FieldInfo field in this.GetType().GetFields(bindingFlags))
		{
			if (field.FieldType.IsSubclassOf (typeof(BaseModel))) {
				o.AddField (field.Name, ((BaseModel)field.GetValue(this)).ToJsonObject());
			} else {
				o.AddField (field.Name, field.GetValue(this).ToString());
			}
		}

		return o.ToString ();
	}

	public virtual void UpdateFromJson(string json){
		BindingFlags bindingFlags = BindingFlags.Public |
			BindingFlags.NonPublic |
			BindingFlags.Instance |
			BindingFlags.Static;



		JSONObject jj  = new JSONObject(json);
		Dictionary<string, string> jsonDict = new Dictionary<string, string> ();

//		foreach (var item in jj.keys) {
//			jsonDict.Add (item, jj [item].ToString ());	
//		}
//		JSONNode jsonObj = JSON.Parse(json);

		string fieldString = "";
		JSONObject jObject = new JSONObject ();


		//update base value
		foreach (FieldInfo field in this.GetType().BaseType.GetFields(bindingFlags))
		{
//			System.Type t = field.FieldType;
//			PropertyInfo prop = this.GetType().BaseType.GetProperty(field.Name);
//			object b= (prop.GetCustomAttributes(typeof(DefaultValueAttribute), true));
//				object a= (DefaultValueAttribute)(prop.GetCustomAttributes(typeof(DefaultValueAttribute), true).GetValue(0));
//
			if (!jj.HasField (field.Name))
				continue;
//			if (EqualityComparer<(t)>.Default.Equals (obj, default(T))) {
//				continue;
//			}
			if (field.FieldType.IsSubclassOf (typeof(BaseModel))) {

				jObject = jj.GetField (field.Name);
				if (jObject.IsNull)
					continue;
				UpdateFromObject (jObject, field);
			} 
			else if(field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition() == typeof(List<>)){
				jObject = jj.GetField (field.Name);
				if (jObject.IsNull)
					continue;
				UpdateFromList (jObject, field);

			}else {
				if (field.FieldType == typeof(string)) {
					jj.GetField(out fieldString, field.Name, fieldString);
					field.SetValue(this,fieldString);
				} else {
					fieldString = jj[field.Name].ToString();
					if (fieldString.Equals ("null")) {
					} else {
						field.SetValue(this,TypeDescriptor.GetConverter(field.FieldType).ConvertFromString(fieldString));
					}
				}
			}
		}


		//update this type value
		foreach (FieldInfo field in this.GetType().GetFields(bindingFlags))
		{


			if (field.FieldType.IsSubclassOf (typeof(BaseModel))) {

				jObject = jj.GetField (field.Name);
				if (jObject.IsNull)
					continue;
				UpdateFromObject (jObject, field);
			} 
			else if(field.FieldType.IsGenericType && field.FieldType.GetGenericTypeDefinition() == typeof(List<>)){
				jObject = jj.GetField (field.Name);
				if (jObject.IsNull)
					continue;
				UpdateFromList (jObject, field);

			}else {
				if (field.FieldType == typeof(string)) {
					jj.GetField(out fieldString, field.Name, fieldString);
					field.SetValue(this,fieldString);
				} else {
					fieldString = jj[field.Name].ToString();
					if (fieldString.Equals ("null")) {
					} else {
						field.SetValue(this,TypeDescriptor.GetConverter(field.FieldType).ConvertFromString(fieldString));
					}
				}
			}
		}
	} 

//	bool IsDefaultValue(object obj, T x){
//		if(EqualityComparer<T>.Default.Equals(obj, default(T))) {
//			return true;
//		}
//	}

	//update model list from json list
	void UpdateFromList(JSONObject jObject, FieldInfo field){
		Type itemType = field.FieldType.GetGenericArguments ()[0];

		IList listToAdd;
		Type genericListType = typeof(List<>).MakeGenericType(itemType);
		listToAdd = (IList)Activator.CreateInstance(genericListType);

		if (itemType.IsSubclassOf ((typeof(BaseModel)))) {

			foreach (var item in jObject.list) {
				BaseModel md = ModelFactory.CreateModel (itemType.Name);
				md.UpdateFromJson (item.ToString ());
				listToAdd.Add (md);

			}
		} else {
			foreach (var item in jObject.list) {
				string tmp1 = item.ToString ();
				listToAdd.Add (TypeDescriptor.GetConverter (itemType).ConvertFromString (item.ToString()));
			}
		}

		field.SetValue (this, listToAdd);
	}

	void UpdateFromObject(JSONObject jObject, FieldInfo field){
		BaseModel md = ModelFactory.CreateModel (field.FieldType.Name);
		md.UpdateFromJson (jObject.ToString());

		field.SetValue (this, md);
	}


	public JSONObject ToJsonObject(){
		BindingFlags bindingFlags = BindingFlags.Public |
			BindingFlags.NonPublic |
			BindingFlags.Instance |
			BindingFlags.Static;
		JSONObject o = new JSONObject ();
		foreach (FieldInfo field in this.GetType().GetFields(bindingFlags))
		{
			if (field.FieldType.IsSubclassOf (typeof(BaseModel))) {
				o.AddField (field.Name, ((BaseModel)field.GetValue(this)).ToJsonObject());
			} else {
				o.AddField (field.Name, field.GetValue(this).ToString());
			}
		}

		return o;
	}


}

public class ServerResponseModel : BaseModel{
	protected string status;
	protected string message;
	protected int code;

	public string Status
	{
		get { return status; }
		set { status = value; }
	}

	public string Message
	{
		get { return message; }
		set { message = value; }
	}

	public int Code
	{
		get { return code; }
		set { code = value; }
	}

	public bool IsSuccess(){
		return code == 1;
	}
}




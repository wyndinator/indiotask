﻿using UnityEngine;
using System.Collections;

public class RegisterModel : BaseModel {
	string facebookToken;
	public string FbToken
	{
		get { return facebookToken; }
		set { facebookToken = value; }
	}

}

public class LoginModel : BaseModel {
	string facebookID;
	public string FacebookID
	{
		get { return facebookID; }
		set { facebookID = value; }
	}

	string token;
	public string Token
	{
		get { return token; }
		set { token = value; }
	}

}

public class PostQuizRequestModel:BaseModel{
	string token;
	string url;
	int type;
	string content;
	string option0, option1, option2, option3;
	string explain;
	int answer;

	public PostQuizRequestModel(string serverToken, string imageUrl,string quizContent, 
		string op1, string op2, string op3, string op4, string quizExplain, int quizAnswer, int quiztype =0){

		token = serverToken;
		url = imageUrl;
		content = quizContent;
		option0 = op1;
		option1 = op2;
		option2 = op3;
		option3 = op4;
		explain = quizExplain;
		type = quiztype;
		answer = quizAnswer;
	}

	public bool IsValidQuiz(){
		return true;
	}
}


public class AnsQuizRequestModel:BaseModel{
	string token;
	string questionID;
	int value;

	public AnsQuizRequestModel(string serverToken, string quesID, int ansIndex){

		token = serverToken;
		questionID = quesID;
		value = ansIndex;
	}	
}

public class GetQuizRequestModel:BaseModel{
	string token;
	int index;

	public GetQuizRequestModel(string serverToken, int quizIndex =0){

		token = serverToken;
		index = quizIndex;
	}	
}

public class GetQuizStatisticRequestModel:BaseModel{
	string token;
	string questionID;

	public GetQuizStatisticRequestModel(string serverToken, string quesID){

		token = serverToken;
		questionID = quesID;
	}	
}

public class LikeQuizRequestModel:BaseModel{
	string token;
	string questionID;

	public LikeQuizRequestModel(string serverToken, string quesID){

		token = serverToken;
		questionID = quesID;
	}	
}

public class ReportQuizRequestModel:BaseModel{
	string token;
	string questionID;

	public ReportQuizRequestModel(string serverToken, string quesID){

		token = serverToken;
		questionID = quesID;
	}	
}

public class UpdateOneSignalModel:BaseModel{
	string token;
	string onesignalID;

	public UpdateOneSignalModel(string serverToken, string signalID){

		token = serverToken;
		onesignalID = signalID;
	}	
}

public class GetConfigRequestModel:BaseModel{
	string token;

	public GetConfigRequestModel(string serverToken){

		token = serverToken;
	}	
}

public class GetGameNewsRequestModel:BaseModel{
	string token;

	public GetGameNewsRequestModel(string serverToken){

		token = serverToken;
	}	
}

﻿using System;

public class ModelFactory
{
	public static BaseModel CreateModel(string typeName){

		switch (typeName) {
		case "ServerRegisterResponseModel": 
			return new ServerRegisterResponseModel ();
		case "UserModel": 
			return new UserModel ();
		case "QuizModel": 
			return new QuizModel ();
		case "QuizAnsModel": 
			return new QuizAnsModel ();
		case "QuizStatisticModel": 
			return new QuizStatisticModel ();

//		case "HelpConfigModel": 
//			return new HelpConfigModel ();

		case "AboutConfigModel": 
			return new AboutConfigModel ();

		case "AdConfigModel": 
			return new AdConfigModel ();

		case "ConfigModel": 
			return new ConfigModel ();
		case "GameNewsModel": 
			return new GameNewsModel ();
		}

		Utility.DebugError ("not found model for type: " + typeName);
		return new ServerRegisterResponseModel ();
	}

//	public static BaseModel CreateResponseModel(string command){
//		switch (command) {
//		case GameCommand.Login:
//			return new ServerRegisterResponseModel ();
//		case GameCommand.Register:
//			return new ServerRegisterResponseModel ();
//		}
//
//		return new ServerResponseModel ();
//	}
}

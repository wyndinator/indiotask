﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ServerRegisterResponseModel : ServerResponseModel{
	UserModel data;


	public UserModel ResponseModelData
	{
		get { return data; }
		set { data = value; }
	}

}

public class QuizStatisticResponseModel : ServerResponseModel{
	QuizStatisticModel data;
	public QuizStatisticModel StatisticData
	{
		get { return data; }
		set { data = value; }
	}
}

public class GetQuizResponseModel : ServerResponseModel{
	List<QuizModel> data;

	public List<QuizModel> ResponseModelData
	{
		get { return data; }
		set { data = value; }
	}
}

public class GetConfigResponseModel : ServerResponseModel{
	ConfigModel data;
	public ConfigModel Data
	{
		get { return data; }
		set { data = value; }
	}
}

public class GetGameNewsResponseModel : ServerResponseModel{
	List<GameNewsModel> data;
	public List<GameNewsModel> Data
	{
		get { return data; }
		set { data = value; }
	}
}